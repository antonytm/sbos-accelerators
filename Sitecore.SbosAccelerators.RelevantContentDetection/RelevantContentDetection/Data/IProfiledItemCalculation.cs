﻿namespace Sitecore.SbosAccelerators.RelevantContentDetection.Data
{
    public interface IProfiledItemCalculation  
    {
        double GetResult();
    }

}
