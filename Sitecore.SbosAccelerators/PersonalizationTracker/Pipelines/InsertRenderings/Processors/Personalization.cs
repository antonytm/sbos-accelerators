﻿namespace Sitecore.SbosAccelerators.PersonalizationTracker.Pipelines.InsertRenderings.Processors
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Reflection;
  using Sitecore.Analytics;
  using Sitecore.Analytics.Data;
  using Sitecore.Configuration;
  using Sitecore.Data;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.Globalization;
  using Sitecore.Layouts;
  using Sitecore.Pipelines.InsertRenderings;
  using Sitecore.Rules;
  using Sitecore.Rules.ConditionalRenderings;
  using Sitecore.Rules.Conditions;
  using Sitecore.StringExtensions;
  using Sitecore.Web;

  public class Personalization : InsertRenderingsProcessor
  {
    protected readonly bool isAnalyticsEnabled = Settings.Analytics.Enabled;

    protected virtual void Evaluate(InsertRenderingsArgs args, Item item)
    {
      var list = new List<RenderingReference>(args.Renderings);
      foreach (RenderingReference reference in list)
      {
        this.RunPersonalization(args, item, reference);
      }
    }

    protected virtual void EvaluatePersonalizationRules(InsertRenderingsArgs args, Item item, RenderingReference rendering)
    {
      Assert.ArgumentNotNull(args, "args");
      Assert.ArgumentNotNull(item, "item");
      Assert.ArgumentNotNull(rendering, "rendering");
      RuleList<ConditionalRenderingsRuleContext> rules = rendering.Settings.Rules;
      var ruleContext = new ConditionalRenderingsRuleContext(args.Renderings, rendering)
      {
        Item = item
      };
      if (Context.PageMode.IsPageEditor)
      {
        Rule<ConditionalRenderingsRuleContext> rule = null;
        if (this.isAnalyticsEnabled && !string.IsNullOrEmpty(rendering.UniqueId))
        {
          ID renderingId = ID.Parse(rendering.UniqueId);
          ID activeRuleId = null; //WebEditUtil.GetPersistedRuleId(renderingId, item);

          string cookieName = string.Format("pe_conditions_{0}_{1}", item.ID.ToShortID(), item.Language);
          string cookieValue = WebUtil.GetCookieValue(cookieName, string.Empty);
          if (cookieValue != null)
          {
            string[] underscoreArray = cookieValue.Split(new[] { '_' });
            string renderingIdString = renderingId.ToShortID().ToString();
            foreach (string str3 in underscoreArray)
            {
              string[] atSignArray = str3.Split(new[] { '@' });
              if ((atSignArray.Length == 2) && string.Equals(atSignArray[0], renderingIdString, StringComparison.OrdinalIgnoreCase))
              {
                activeRuleId = ID.Decode(atSignArray[1]);
                break;
              }
            }
          }

          if (!ID.IsNullOrEmpty(activeRuleId))
          {
            Func<Rule<ConditionalRenderingsRuleContext>, bool> predicate = r => r.UniqueId == activeRuleId;
            rule = rules.Rules.FirstOrDefault(predicate);
          }
        }
        if (rule == null)
        {
          rule = rules.Rules.FirstOrDefault(r => r.UniqueId == ItemIDs.Analytics.DefaultCondition);
        }
        if (rule != null)
        {
          rule.SetCondition(new TrueCondition<ConditionalRenderingsRuleContext>());
          new RuleList<ConditionalRenderingsRuleContext>(rule).Run(ruleContext);
        }
      }
      else
      {
        rules.RunFirstMatching(ruleContext);

        this.TriggerPersonalizationEvents(args, item, rendering, rules);
      }
    }

    private void TriggerPersonalizationEvents(InsertRenderingsArgs args, Item item, RenderingReference rendering, RuleList<ConditionalRenderingsRuleContext> rules)
    {
      try
      {
        using (new LanguageSwitcher("en"))
        {
          Database database = Context.Database;
          Item petCategory = database.GetItem(PetIDs.PetPageEventCategory);
          if (petCategory != null)
          {
            if (petCategory.Children != null)
            {
              Item categoryItem = petCategory.Children.SingleOrDefault(i => i.DisplayName == Context.Item.Name);
              if (categoryItem != null)
              {
                IEnumerable<Item> viewEvents = categoryItem.Children.Where(i => i.DisplayName.Contains("_View"));
                if (viewEvents != null)
                {
                  IEnumerable<Item> pageEvents = viewEvents.Where(i => i.DisplayName.Contains(rendering.UniqueId.Trim(new[] { '{', '}' }))).ToList();

                  foreach (Rule<ConditionalRenderingsRuleContext> rule in rules.Rules)
                  {
                    var stack = new RuleStack();
                    var anotherRuleContext = new ConditionalRenderingsRuleContext(args.Renderings, rendering)
                    {
                      Item = item
                    };
                    if (rule.Condition != null)
                    {
                      rule.Condition.Evaluate(anotherRuleContext, stack);

                      if (pageEvents != null)
                      {
                        var pageEvent = pageEvents.FirstOrDefault(i => i.Name.Contains(rule.UniqueId.ToString().Trim(new[] { '{', '}' })));
                        if ((bool)stack.Peek() && pageEvent != null)
                        {
                          string[] splittedPageEventName = pageEvent.DisplayName.Split(new[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                          Item renderingItem = database.GetItem(rendering.RenderingItem.ID);
                          if (renderingItem != null)
                          {
                            var pageEventData = new PageEventData(pageEvent.Name)
                            {
                              Data = renderingItem.Name + " | {" + splittedPageEventName[0] + "}",
                              DataKey = splittedPageEventName[1] + "_" + splittedPageEventName[2],
                              Text = splittedPageEventName[3],
                              ItemId = item.ID.ToGuid()
                            };

                            Context.ClientData.SetValue(rendering.RenderingItem.InnerItem["Path"], "True");
                            Context.ClientData.SetValue(rendering.UniqueId, pageEvent.Name);

                            if (Tracker.IsActive)
                            {
                              Tracker.CurrentPage.Register(pageEventData);
                              string mtmvtData = "<p ruid=\"{0}\">{1}</p>".FormatWith(rendering.UniqueId.Trim(new[] { '{', '}' }), splittedPageEventName[1] + "_" + splittedPageEventName[2]);
                              Tracker.CurrentVisit.CurrentPage.Data += mtmvtData;
                              //Tracker.Submit();
                            }
                          }

                          return;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      catch (Exception exception)
      {
        Log.Error("[Sitecore.SbosAccelerators.PersonalizationTracker] Exception in Personalization processor: " + exception.Message, exception, this);
      }
    }

    public override void Process(InsertRenderingsArgs args)
    {
      Assert.ArgumentNotNull(args, "args");
      if ((args.EvaluateConditions && args.HasRenderings) && !args.IsDesigning)
      {
        Item contextItem = args.ContextItem;
        if (contextItem != null)
        {
          this.Evaluate(args, contextItem);
        }
      }
    }

    private void RunPersonalization(InsertRenderingsArgs args, Item item, RenderingReference rendering)
    {
      Assert.ArgumentNotNull(args, "args");
      Assert.ArgumentNotNull(item, "item");
      Assert.ArgumentNotNull(rendering, "rendering");
      try
      {
        PropertyInfo property = args.GetType().GetProperty("TestingRenderingUniqueIds", BindingFlags.Instance | BindingFlags.NonPublic);
        var propertyValue = property.GetValue(args, null) as List<string>;

        if (propertyValue != null && ((propertyValue.IndexOf(rendering.UniqueId) < 0) && (rendering.Settings.Rules.Count > 0)))
        {
          this.EvaluatePersonalizationRules(args, item, rendering);
        }
      }
      catch (Exception exception)
      {
        Log.Error("Execution of personalization rules failed. ItemID: {0},  Placeholder name: {1}, Rendering Item ID: {2}, Rendering Item name: {3}".FormatWith(new object[]
        {
          item.ID, rendering.Placeholder, rendering.RenderingID, rendering.RenderingItem.Name
        }), exception, this);
      }
    }
  }
}
