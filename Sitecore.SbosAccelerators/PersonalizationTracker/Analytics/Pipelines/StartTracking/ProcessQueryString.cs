namespace Sitecore.SbosAccelerators.PersonalizationTracker.Analytics.Pipelines.StartTracking
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Xml.Linq;
  using Sitecore.Analytics;
  using Sitecore.Analytics.Configuration;
  using Sitecore.Analytics.Data;
  using Sitecore.Analytics.Data.DataAccess.DataSets;
  using Sitecore.Analytics.Data.Items;
  using Sitecore.Analytics.Extensions;
  using Sitecore.Analytics.Pipelines.StartTracking;
  using Sitecore.Collections;
  using Sitecore.Configuration;
  using Sitecore.Data;
  using Sitecore.Data.Fields;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.Layouts;
  using Sitecore.StringExtensions;
  using Sitecore.Web;

  public class ProcessQueryString : Sitecore.Analytics.Pipelines.StartTracking.ProcessQueryString
  {
    private readonly SafeDictionary<string, string> map = new SafeDictionary<string, string>();

    public override void Process(StartTrackingArgs args)
    {
      Assert.ArgumentNotNull(args, "args");
      string str = WebUtil.GetQueryString(AnalyticsSettings.EventQueryStringKey).Trim();
      if (!string.IsNullOrEmpty(str))
      {
        try
        {
          TriggerEvent(str);
        }
        catch (Exception exception)
        {
          Log.Error(string.Concat(new object[] { "Failed to trigger event from query string: ", str, ". Exception: ", exception }), this);
        }
      }
      string str2 = WebUtil.GetQueryString(Settings.GetSetting("Analytics.CampaignQueryStringKey")).Trim();
      if (!string.IsNullOrEmpty(str2))
      {
        TriggerCampaign(str2);
      }
      this.TriggerQueryString();
    }

    private static void TriggerCampaign(string campaign)
    {
      CampaignItem item;
      Assert.ArgumentNotNull(campaign, "campaign");
      if (ShortID.IsShortID(campaign))
      {
        ID id = ShortID.DecodeID(campaign);
        item = Tracker.DefinitionItems.Campaigns[id];
      }
      else if (ID.IsID(campaign))
      {
        var id2 = new ID(campaign);
        item = Tracker.DefinitionItems.Campaigns[id2];
      }
      else
      {
        item = Tracker.DefinitionItems.Campaigns[campaign];
      }
      if (item == null)
      {
        Log.Warn("Campaign not found: " + campaign, typeof(ProcessQueryString));
      }
      else
      {
        Tracker.CurrentPage.TriggerCampaign(item);
      }
    }

    private static void TriggerEvent(string eventData)
    {
      Assert.ArgumentNotNull(eventData, "eventData");
      PageEventData data = PageEventData.Parse(eventData);
      if (eventData.Contains("_Click") || eventData.Contains("_View"))
      {
        string[] splittedPageEventName = eventData.Split(new[]
        {
          '_'
        }, StringSplitOptions.RemoveEmptyEntries);
        object itemObject = Context.ClientData.GetValue(eventData);
        // This is PT
        if (itemObject != null)
        {
          // This is Personalization Tracker:
          // splittedPageEventName[0] - rendering ID
          // splittedPageEventName[1] - index
          // splittedPageEventName[2] - condition ID
          // splittedPageEventName[3] - 'Click'

          string itemId = itemObject.ToString();
          data.ItemId = string.IsNullOrEmpty(itemId) ? Tracker.CurrentVisit.PreviousPage.ItemId : new Guid(itemId);

          try
          {
            Item item = Context.Database.GetItem(new ID(itemId));
            item.Fields.ReadAll();
            LayoutField layoutField = item.Fields[FieldIDs.LayoutField];
            LayoutDefinition layoutDefinition = LayoutDefinition.Parse(layoutField.Value);

            foreach (DeviceDefinition deviceDefinition in layoutDefinition.Devices)
            {
              RenderingDefinition renderingDefinition = deviceDefinition.GetRenderingByUniqueId("{" + splittedPageEventName[0] + "}");
              if (renderingDefinition != null)
              {
                Item renderingItem = item.Database.GetItem(ID.Parse(renderingDefinition.ItemID));

                var rule = new List<string>();
                int ruleIndex = 0;

                foreach (XNode r in renderingDefinition.Rules.Nodes())
                {
                  ruleIndex++;
                  var xAttribute = ((XElement)r).Attribute("uid");
                  if (xAttribute != null && xAttribute.Value == "{" + splittedPageEventName[2] + "}")
                  {
                    var attribute = ((XElement)r).Attribute("name");
                    if (attribute != null)
                    {
                      rule.Add(attribute.Value);
                    }
                    break;
                  }
                }

                data.DataKey = ruleIndex.ToString("D2") + "_" + rule[0];
                data.Data = renderingItem.Name + " | {" + splittedPageEventName[0] + "}";
                break;
              }
            }
          }
          catch
          {
            data.DataKey = splittedPageEventName[1] + "_" + splittedPageEventName[1];
            data.Data = splittedPageEventName[0];
          }
          data.Text = splittedPageEventName[3];

          Tracker.CurrentVisit.PreviousPage.Register(data);
        }
        else
        {
          // This is MTMVT:
          // splittedPageEventName[0] - item ID
          // splittedPageEventName[1] - rendering ID
          // splittedPageEventName[2] - variation name
          // splittedPageEventName[3] - page ID
          // splittedPageEventName[4] - 'Click'

          data.ItemId = new Guid(splittedPageEventName[0]);
          data.DataKey = splittedPageEventName[2];
          data.Data = splittedPageEventName[1];
          data.Text = splittedPageEventName[4];
          data.Name = "{0}_{1}_Click".FormatWith(splittedPageEventName[1], splittedPageEventName[2]);

          VisitorDataSet.PagesRow page = Tracker.Visitor.DataSet.Pages.FindByPageId(new Guid(splittedPageEventName[3]));

          if (page != null)
          {
            page.Register(data);
          }
          else
          {
            Tracker.CurrentVisit.PreviousPage.Register(data);
          }
        }
      }
      else
      {
        Tracker.CurrentPage.Register(data);
      }
    }

    private void TriggerQueryString()
    {
      foreach (string str in this.map.Keys)
      {
        if (!string.IsNullOrEmpty(str))
        {
          string queryString = WebUtil.GetQueryString(str, null);
          if (queryString != null)
          {
            string str3 = this.map[str];
            if (!string.IsNullOrEmpty(str3))
            {
              Assert.ArgumentNotNull(str3, "name");
              Assert.ArgumentNotNull(queryString, "value");
              string argument = queryString;
              string str5 = queryString;
              string str6 = queryString;
              Assert.ArgumentNotNull(str3, "name");
              Assert.ArgumentNotNull(argument, "text");
              Assert.ArgumentNotNull(str6, "dataKey");
              Assert.ArgumentNotNull(str5, "data");
              var data = new PageEventData(str3)
              {
                Text = argument,
                DataKey = str6,
                Data = str5,
                ItemId = (Context.Item == null) ? Guid.Empty : Context.Item.ID.ToGuid()
              };
              Tracker.CurrentPage.Register(data);
            }
          }
        }
      }
    }
  }
}
