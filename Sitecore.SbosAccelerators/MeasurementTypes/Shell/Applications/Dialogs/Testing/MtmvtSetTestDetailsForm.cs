// -----------------------------------------------------------------------
// <copyright file="MtmvtSetTestDetailsForm.cs" company="Sitecore A/S">
// Copyright � 2013 by Sitecore A/S
// </copyright>
// -----------------------------------------------------------------------

namespace Sitecore.SbosAccelerators.MeasurementTypes.Shell.Applications.Dialogs.Testing
{
  using System;
  using System.Collections.Generic;
  using System.Collections.Specialized;
  using System.Linq;
  using Sitecore.Analytics;
  using Sitecore.Analytics.Data.Items;
  using Sitecore.Analytics.Testing.TestingUtils;
  using Sitecore.Data;
  using Sitecore.Data.Fields;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.Globalization;
  using Sitecore.Layouts;
  using Sitecore.SbosAccelerators.MeasurementTypes.Enums;
  using Sitecore.SbosAccelerators.MeasurementTypes.Utils;
  using Sitecore.Shell.Applications.Dialogs.Testing;
  using Sitecore.Web;
  using Sitecore.Web.UI.HtmlControls;
  using Sitecore.Web.UI.Sheer;

  /// <summary>
  /// Extends default SetTestDetailsForm with new features.
  /// </summary>
  public class MtmvtSetTestDetailsForm : SetTestDetailsForm
  {
    /// <summary>
    /// Gets or sets Goals border.
    /// </summary>
    protected Border GoalsBorder { get; set; }

    /// <summary>
    /// Gets or sets a list with measurement types.
    /// </summary>
    protected Combobox MeasurementTypesCombobox { get; set; }

    /// <summary>
    /// Gets or sets a list with goals from %goal root%.
    /// </summary>
    protected Combobox GoalsCombobox { get; set; }

    /// <summary>
    /// Handles 'selection changed' event of the MeasurementType combo box.
    /// </summary>
    public void MeasurementTypeChanged()
    {
      this.GoalsBorder.Visible = this.MeasurementTypesCombobox.SelectedItem.Value.Equals(MtmvtIDs.SpecificGoalItem.ToString(), StringComparison.OrdinalIgnoreCase);
    }

    /// <summary>
    /// Handles Load event. Initializes goals and measurement types combo boxes with initial values.
    /// </summary>
    /// <param name="e">Event arguments.</param>
    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      if (!Context.ClientPage.IsEvent)
      {
        Item goalsRoot = Context.ContentDatabase.GetItem(AnalyticsIds.GoalsRoot, Language.Parse("en"));

        if (goalsRoot != null)
        {
          var allGoals = goalsRoot.Axes.GetDescendants().Where(desc => desc.TemplateID == AnalyticsIds.Goal || desc.TemplateID == AnalyticsIds.PageEvent);
          foreach (Item child in allGoals)
          {
            var goal = new ListItem
            {
              Header = child.DisplayName,
              Value = child.ID.ToString()
            };
            this.GoalsCombobox.Controls.Add(goal);
          }
        }

        if (this.Rendering != null)
        {
          DeviceDefinition device = this.Device;
          Assert.IsNotNull(device, "device");
          MultivariateTestDefinitionItem testDefinition = TestingUtil.MultiVariateTesting.GetTestDefinition(device, new ID(this.RenderingUniqueId), Client.ContentDatabase);

          if (testDefinition != null)
          {
            testDefinition.InnerItem.Fields.ReadAll();

            string measurementTypeId = testDefinition.InnerItem[MtmvtIDs.MeasurementTypeField];

            if (string.IsNullOrEmpty(measurementTypeId))
            {
              measurementTypeId = MtmvtIDs.EngagementValueItem.ToString();
            }

            string goalId = testDefinition.InnerItem[MtmvtIDs.SelectedGoalField];

            this.MeasurementTypesCombobox.Value = measurementTypeId;
            this.GoalsBorder.Visible = measurementTypeId.Equals(MtmvtIDs.SpecificGoalItem.ToString(), StringComparison.OrdinalIgnoreCase);
            if (this.GoalsBorder.Visible)
            {
              this.GoalsCombobox.Value = goalId;
            }
          }
        }
      }
    }

    /// <summary>
    /// Handles form closing with OK button. Sets valid measurement type on test definition item.
    /// </summary>
    /// <param name="sender">Event sender.</param>
    /// <param name="args">Event arguments.</param>
    protected override void OnOK(object sender, EventArgs args)
    {
      DeviceDefinition device = this.Device;
      Assert.IsNotNull(device, "device");
      MultivariateTestDefinitionItem testDefinition = TestingUtil.MultiVariateTesting.GetTestDefinition(device, new ID(this.RenderingUniqueId), Client.ContentDatabase);
      if (testDefinition == null)
      {
        ItemUri contextItemUri = this.ContextItemUri;
        if (contextItemUri == null)
        {
          return;
        }

        Item item = Client.ContentDatabase.GetItem(contextItemUri.ToDataUri());
        if (item != null)
        {
          testDefinition = TestingUtil.MultiVariateTesting.AddTestDefinition(item);
        }
      }

      if (testDefinition == null)
      {
        SheerResponse.Alert("The action cannot be executed because of security restrictions.", new string[0]);
      }
      else if (this.Rendering != null)
      {
        MultivariateTestVariableItem variableItem = TestingUtil.MultiVariateTesting.GetVariableItem(this.Rendering, Client.ContentDatabase)
                                                 ?? TestingUtil.MultiVariateTesting.AddVariable(testDefinition, this.Rendering, Client.ContentDatabase);
        if (variableItem == null)
        {
          SheerResponse.Alert("The action cannot be executed because of security restrictions.", new string[0]);
        }
        else
        {
          string measurementTypeId = this.MeasurementTypesCombobox.SelectedItem.Value;
          Item measurementTypeItem = Context.ContentDatabase.GetItem(ID.Parse(measurementTypeId));

          MeasurementType parsedSelectedValue = (MeasurementType)Enum.Parse(typeof(MeasurementType), measurementTypeItem.Name);
          using (new EditContext(testDefinition.InnerItem))
          {
            testDefinition.InnerItem.Fields[MtmvtIDs.MeasurementTypeField].SetValue(measurementTypeId, true);

            if (parsedSelectedValue == MeasurementType.SpecificGoal)
            {
              testDefinition.InnerItem.Fields[MtmvtIDs.SelectedGoalField].SetValue(this.GoalsCombobox.SelectedItem.Value, true);
            }
          }
          
          if (parsedSelectedValue == MeasurementType.ClickThroughRate || parsedSelectedValue == MeasurementType.SpecificGoal)
          {
            Item contextItem = Client.ContentDatabase.GetItem(this.ContextItemUri.ToDataUri());

            LayoutDefinition layout = LayoutDefinition.Parse(LayoutField.GetFieldValue(contextItem.Fields[FieldIDs.LayoutField]));

            foreach (RenderingDefinition renderingDefinition in device.Renderings)
            {
              if (renderingDefinition.UniqueId == this.RenderingUniqueId)
              {
                NameValueCollection parameters = StringUtil.ParseNameValueCollection(renderingDefinition.Parameters ?? "", '&', '=');
                parameters["XmvtRenderingID"] = this.RenderingUniqueId;
                renderingDefinition.Parameters = StringUtil.NameValuesToString(parameters, "&").Replace(", ", "&");
              }
            }

            int index = layout.Devices.IndexOf(layout.Devices.OfType<DeviceDefinition>().First(dev => string.Equals(dev.ID, device.ID, StringComparison.OrdinalIgnoreCase)));
            layout.Devices[index] = device;

            WebUtil.SetSessionValue(this.LayoutSessionHandle, layout.ToXml());
          }

          List<ID> modifiedVariations;
          if (!this.UpdateVariableValues(variableItem, out modifiedVariations))
          {
            SheerResponse.Alert("The action cannot be executed because of security restrictions.", new string[0]);
          }
          else
          {
            SheerResponse.SetDialogValue(SetTestDetailsOptions.GetDialogResut(variableItem.ID, modifiedVariations));
            SheerResponse.CloseWindow();
          }
        }
      }
    }
  }
}
