﻿namespace Sitecore.SbosAccelerators.MeasurementTypes.Shell.Applications.WebEdit.Commands
{
  using System;
  using Sitecore.Analytics.Testing.TestingUtils;
  using Sitecore.Data;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.SbosAccelerators.MeasurementTypes.Enums;
  using Sitecore.SbosAccelerators.MeasurementTypes.Utils;
  using Sitecore.Shell.Applications.WebEdit.Commands.Testing;
  using Sitecore.Shell.Framework.Commands;
  using Sitecore.Text;
  using Sitecore.Web;
  using Sitecore.Web.UI.Sheer;

  public class ShowReport : StopTest
  {
    public override void Execute(CommandContext context)
    {
      Assert.ArgumentNotNull(context, "context");
      if (context.Items.Length == 1)
      {
        var str = new UrlString("/sitecore/shell/applications/analytics/default.aspx");
        Item item = context.Items[0];
        
        if (item != null)
        {
          ID clientDeviceId = WebEditUtil.GetClientDeviceId();
          Item testDefinition = TestingUtil.MultiVariateTesting.GetTestDefinition(item, clientDeviceId);

          string measurementTypeId = testDefinition[MtmvtIDs.MeasurementTypeField];

          if (string.IsNullOrEmpty(measurementTypeId))
          {
            str["fo"] = MtmvtIDs.ReportIDs.ByComponentEngagement.ToString(); // 'by component' report
            str["ro"] = MtmvtIDs.ReportIDs.ByComponentEngagement.ToString(); // 'by component' report
          }
          else
          {
            MeasurementType measurement = (MeasurementType)Enum.Parse(typeof(MeasurementType), Context.ContentDatabase.GetItem(ID.Parse(measurementTypeId)).Name);

            switch (measurement)
            {
              default:
                str["fo"] = MtmvtIDs.ReportIDs.ByComponentEngagement.ToString(); // 'by component' report
                str["ro"] = MtmvtIDs.ReportIDs.ByComponentEngagement.ToString(); // 'by component' report
                break;
              case MeasurementType.SpecificGoal:
                string goalId = testDefinition[MtmvtIDs.SelectedGoalField];
                str["fo"] = MtmvtIDs.ReportIDs.ByComponentConversions.ToString(); // 'by conversion' report
                str["ro"] = MtmvtIDs.ReportIDs.ByComponentConversions.ToString(); // 'by conversion' report
                str["goalid"] = goalId;
                break;
              case MeasurementType.ClickThroughRate:
                str["fo"] = MtmvtIDs.ReportIDs.ByComponentCtr.ToString(); // 'ctr' report
                str["ro"] = MtmvtIDs.ReportIDs.ByComponentCtr.ToString(); // 'ctr' report
                break;
            }
          }
          
          str["testid"] = testDefinition.ID.ToString();
          SheerResponse.Eval(string.Concat(new object[]
          {
            "window.open('", str, "', 'SitecoreWebEditEditor', 'location=0,menubar=0,status=0,toolbar=0,resizable=1')"
          }));
        }
      }
    }
  }
}
