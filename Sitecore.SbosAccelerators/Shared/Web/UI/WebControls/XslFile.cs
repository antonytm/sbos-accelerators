namespace Sitecore.SbosAccelerators.Shared.Web.UI.WebControls
{
  using System;
  using System.Collections.Generic;
  using System.IO;
  using System.Linq;
  using System.Text;
  using System.Web.UI;
  using System.Web.UI.HtmlControls;
  using HtmlAgilityPack;
  using Sitecore.Configuration;
  using Sitecore.Diagnostics;
  using Sitecore.SbosAccelerators.Shared.PageExtenders;
  using Sitecore.Text;
  using Sitecore.Web;

  public class XslFile : Sitecore.Web.UI.WebControls.XslFile
  {
    public string PetActive { get; set; }

    public string PetRenderingID { get; set; }

    public string XmvtRenderingID { get; set; }

    protected override void Render(HtmlTextWriter output)
    {
      if (string.IsNullOrEmpty(this.PetActive) && string.IsNullOrEmpty(this.XmvtRenderingID))
      {
        base.Render(output);
        return;
      }

      string queryString = string.Empty;

      if (!string.IsNullOrEmpty(this.XmvtRenderingID))
      {
        object value = Sitecore.Context.ClientData.GetValue(this.XmvtRenderingID);

        try
        {
          string pageId = (from control in this.Page.Controls.OfType<HtmlInputHidden>()
                           where string.Equals(control.ID, InsertAnalyticsDataExtender.AnalyticsPageIdKey, StringComparison.OrdinalIgnoreCase)
                           select control.Value).FirstOrDefault();

        if (value != null)
        {
          string variationName = value.ToString();
            queryString = string.Format("{0}_{1}_{2}_Click", pageId, this.XmvtRenderingID.Trim(new[] { '{', '}' }), variationName);
          }
        }
        catch (Exception ex)
        {
          Log.Error(ex.Message, ex, this);
        }
      }

      if (!string.IsNullOrEmpty(this.PetActive) && this.PetActive == "1")
      {
        if (this.PetRenderingID != null)
        {
          var petEvent = Sitecore.Context.ClientData.GetValue(this.PetRenderingID);
          if (petEvent != null)
          {
            queryString = string.IsNullOrEmpty(queryString) ? petEvent.ToString().Replace("_View", "_Click") : queryString + "&" + petEvent.ToString().Replace("_View", "_Click");
            Sitecore.Context.ClientData.SetValue(petEvent.ToString().Replace("_View", "_Click"), Sitecore.Context.Item.ID.ToString());
          }
        }
      }

      var sb = new StringBuilder();
      var myWriter = new HtmlTextWriter(new StringWriter(sb));
      base.Render(myWriter);
      string allHtml = sb.ToString();
      List<SiteInfo> allSites = Factory.GetSiteInfoList();

      var document = new HtmlDocument();
      document.LoadHtml(allHtml);

      try
      {
        foreach (HtmlNode link in document.DocumentNode.SelectNodes("//a[@href]"))
        {
          HtmlAttribute att = link.Attributes["href"];

          var uri = new Uri(att.Value, UriKind.RelativeOrAbsolute);
          string hostName = uri.IsAbsoluteUri ? uri.GetLeftPart(UriPartial.Authority) : string.Empty;
          bool isLocalSite = allSites.Any(si => si.HostName == hostName || si.TargetHostName == hostName);

          if (!string.IsNullOrEmpty(queryString))
          {
            if (isLocalSite)
            {
              var url = new UrlString(att.Value);
              att.Value = url.Add("sc_trk", queryString);
            }
            else
            {
              var url = new UrlString(System.IO.Path.Combine(hostName, "/sitecore modules/Shell/Personalization Tracker/link_to.aspx"));
              url.Add("to", att.Value);
              url.Add("sc_trk", queryString);

              att.Value = url.ToString();
            }
          }
        }
      }
      catch
      {
        document.Save(output);
        return;
      }

      document.Save(output);
    }
  }
}
