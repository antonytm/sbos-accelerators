﻿using Sitecore.Diagnostics;
using Sitecore.SbosAccelerators.MeasurementTypes.Utils;
using Sitecore.StringExtensions;
using Sitecore.Data;

namespace Sitecore.SbosAccelerators.MeasurementTypes.Shell.Utils
{
  using Sitecore.Data.Items;

  public static class MeasurementTypeUtil
  {
    const string DefaultMeasurementTypeName = "Engagement Value";

    public static string GetMeasurementType(ID testDefinitionItemId)
    {
      var contentDatabase = Sitecore.Context.ContentDatabase;
      var testDefinitionItem = contentDatabase.GetItem(testDefinitionItemId);
      var measurementTypeFieldValue = testDefinitionItem[MtmvtIDs.MeasurementTypeField];
      if (string.IsNullOrEmpty(measurementTypeFieldValue) || !ID.IsID(measurementTypeFieldValue))
      {
        measurementTypeFieldValue = MtmvtIDs.EngagementValueItem.ToString();
      }

      var measurementTypeItem = contentDatabase.GetItem(measurementTypeFieldValue);
      if (measurementTypeItem == null)
      {
        Log.Warn("Cannot find the {0} item in the {1} database".FormatWith(measurementTypeFieldValue, contentDatabase.Name), typeof (MeasurementTypeUtil));
        return DefaultMeasurementTypeName;
      }

      var measurementType = measurementTypeItem[MtmvtIDs.MeasurementTypeDisplayNameFieldId];
      if (string.IsNullOrEmpty(measurementType))
      {
        Log.Warn("The {0} field of the {1} item is empty".FormatWith(MtmvtIDs.MeasurementTypeDisplayNameFieldId, measurementTypeItem.Paths.FullPath), typeof (MeasurementTypeUtil));
        return DefaultMeasurementTypeName;
      }

      string goalName = string.Empty;
      if (testDefinitionItem[MtmvtIDs.SelectedGoalField] != string.Empty)
      {
        goalName = contentDatabase.GetItem(ID.Parse(testDefinitionItem[MtmvtIDs.SelectedGoalField])).Name;
      }

      return measurementType.FormatWith(goalName);
    }

    public static string GetGoalId(ID testDefinitionItemId)
    {
      Database contentDatabase = Sitecore.Context.ContentDatabase;
      Item testDefinitionItem = contentDatabase.GetItem(testDefinitionItemId);

      return testDefinitionItem.Fields[MtmvtIDs.SelectedGoalField].Value;
    }
  }
}
