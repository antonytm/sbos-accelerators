namespace Sitecore.SbosAccelerators.PersonalizationTracker.Shell.Personalization
{
  using System;
  using System.Collections.Specialized;
  using System.Web;
  using Sitecore.Diagnostics;
  using Sitecore.Text;
  using Sitecore.Web;
  using Sitecore.Web.UI.HtmlControls;
  using Sitecore.Web.UI.Sheer;
  using Sitecore.Web.UI.WebControls;
  using Sitecore.Web.UI.XmlControls;

  public class PETForm : ApplicationForm
  {
    protected GridPanel Body;
    protected XmlControl ContextMenuView;
    protected DataListview ItemExplorer;
    protected DataContext MainPanelDataContext;
    protected TreeviewEx ItemTreeview;
    protected DataContext TreeDataContext;
    protected Scrollbox Items;

    protected Frame ReportFrame;

    protected override void OnLoad(EventArgs e)
    {
      Assert.ArgumentNotNull(e, "e");
      if (!Context.ClientPage.IsEvent)
      {
        this.MainPanelDataContext.DataViewName = "Master";
        this.MainPanelDataContext.Parameters = "databasename=master";
        this.MainPanelDataContext.Root = ItemIDs.ContentRoot.ToString();
        this.MainPanelDataContext.GetFromQueryString();
        this.TreeDataContext.DataViewName = "Master";
        this.TreeDataContext.Parameters = "databasename=master";
        this.TreeDataContext.Root = ItemIDs.ContentRoot.ToString();
        this.TreeDataContext.GetFromQueryString();
      }
      base.OnLoad(e);

      this.OnSelect(null);
    }

    [HandleMessage("pet:select")]
    public void OnSelect(Message message)
    {
      HttpContext current = HttpContext.Current;
      var url = new UrlString("/sitecore modules/Shell/Personalization Tracker/Apps/PetLayout.aspx"); // TODO: Remove testing
      url["rid"] = "A6118F8C1DDB46BCAB4E580942CFE2B5";
      url["id"] = this.ItemTreeview.GetSelectionItem() != null ? this.ItemTreeview.GetSelectionItem().ID.ToString() :
        string.IsNullOrEmpty(WebUtil.GetQueryString("fo")) ? "00000000-0000-0000-0000-000000000000" : WebUtil.GetQueryString("fo");
      url["w"] = "0";
      if (current != null)
      {
        GetParameters(current, url);
      }
      this.ReportFrame.SourceUri = url.ToString();
    }

    private static void GetParameters(HttpContext httpContext, UrlString url)
    {
      Assert.ArgumentNotNull(httpContext, "httpContext");
      Assert.ArgumentNotNull(url, "url");
      NameValueCollection queryString = httpContext.Request.QueryString;
      foreach (string str in queryString.Keys)
      {
        if ((!string.IsNullOrEmpty(str) && (string.Compare(str, "xmlcontrol", StringComparison.InvariantCultureIgnoreCase) != 0)) && ((string.Compare(str, "fo", StringComparison.InvariantCultureIgnoreCase) != 0) && (string.Compare(str, "ro", StringComparison.InvariantCultureIgnoreCase) != 0)))
        {
          url[str] = queryString[str];
        }
      }
    }
  }
}
