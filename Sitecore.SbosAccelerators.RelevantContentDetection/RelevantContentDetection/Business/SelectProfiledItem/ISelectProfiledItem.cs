﻿using Sitecore.SbosAccelerators.RelevantContentDetection.Business.CalculateItemProfile;
using Sitecore.SbosAccelerators.RelevantContentDetection.Data;

namespace Sitecore.SbosAccelerators.RelevantContentDetection.Business.SelectProfiledItem
{
    public interface ISelectProfiledItem
    {
        string StringKey { get; }

        ICalculateItemProfile CalculateItemProfile { get; }

        IProfiledItemResult GetProfiledItem(IProfiledItemList list);


    }

}
