﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" Inherits="Sitecore.SbosAccelerators.PersonalizationTracker.Shell.Personalization.PETLayout, Sitecore.SbosAccelerators" %>

<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.HtmlControls" TagPrefix="sc" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls.Ribbons" Assembly="Sitecore.Kernel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Sitecore Personalization Effectiveness Tracker</title>
  <sc:Stylesheet ID="Stylesheet1" Src="Content Manager.css" DeviceDependant="true" runat="server" />
  <sc:Stylesheet ID="Stylesheet2" Src="Ribbon.css" DeviceDependant="true" runat="server" />
  <sc:Stylesheet ID="Stylesheet3" Src="Grid.css" DeviceDependant="true" runat="server" />

  <sc:Script Src="/sitecore/shell/Controls/InternetExplorer.js" runat="server" />
  <sc:Script Src="/sitecore/shell/Controls/Sitecore.js" runat="server" />
  <sc:Script Src="/sitecore/shell/Controls/SitecoreObjects.js" runat="server" />
  <sc:Script Src="/sitecore/shell/Applications/Content Manager/Content Editor.js" runat="server" />
  <script src="/sitecore/shell/Controls/Lib/jQuery/jquery.js" type="text/javascript"></script>

  <link href="pet.css" rel="stylesheet" />

  <script type="text/javascript">
    $(document).ready(function () {
      $('#aboutOutput').hide();
      
      $('#aboutSpan').click(function () {
        $('#aboutOutput').toggle(150);
      });
    });
  </script>

</head>
<body>
  <div class="about">
    <img alt="info" style="float: right; display: block" src="/sitecore/shell/Themes/Standard/Images/toolinfo.png" />
    <span id="aboutSpan" style="cursor: help">About this report</span>
    <div id="aboutOutput">
      <hr class="plain" />
      <div>
        Via supplied report one can view effectiveness of various spot combinations per page, e.g.:
        <ul>
          <li>How many times a spot has been shown;</li>
          <li>How many times a user has clicked the spot;</li>
          <li>What value the spot has contributed:
            <ul>
              <li>per view;</li>
              <li>per click;</li>
            </ul>
          </li>
          <li>What specific goals (selected by user) the spot has contributed:
          <ul>
            <li>per view;</li>
            <li>per click.</li>
          </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <form method="post" runat="server" id="mainform" class="wf-container">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="True" />
    <div class="wf-content">
      <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
          <div class="outerProgress">
            <div class="innerProgress">
              Updating report...
            </div>
          </div>
        </ProgressTemplate>
      </asp:UpdateProgress>
      <asp:UpdatePanel ID="updatePanel" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
          <div>
            <div style="text-align: right; float: right; display: block" class="radio">
              <asp:CheckBox ID="showHistoricalCheckBox" AutoPostBack="True" Checked="True" runat="server" Text="Show historical renderings" OnCheckedChanged="ShowHistoricalCheckedChanged" ToolTip="Determines whether the report shows all renderings with personalization data avaialble or only active ones." />
            </div>
            <div style="float: left; display: block">
              <asp:RadioButtonList ID="modeRadiogroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SelectedIndexChanged" CssClass="radio">
                <asp:ListItem Selected="True">Engagement value</asp:ListItem>
                <asp:ListItem ID="specificGoalRadio">Specific goal</asp:ListItem>
              </asp:RadioButtonList>
            </div>
          </div>
          <br style="clear: both" />
          <asp:Panel runat="server" ID="goalDropdownPanel" Visible="False">
            <hr />
            <asp:Label ID="Label1" runat="server" AssociatedControlID="goalDropdown">Select specific goal:</asp:Label>
            <asp:DropDownList runat="server" ID="goalDropdown" AutoPostBack="True" OnSelectedIndexChanged="SelectedGoalChanged" Width="200" />
          </asp:Panel>
          <asp:Repeater ID="outerRepeater" runat="server" OnItemDataBound="RepeaterRowBound">
            <HeaderTemplate>
              <hr />
            </HeaderTemplate>
            <ItemTemplate>
              <table id="rptTable" class="table table-bordered">
                <tbody>
                  <tr>
                    <td colspan="7" style="text-align: center; background-color: #F5F5F5">
                      <strong><%# Eval("RenderingName") %></strong>
                      <br />
                      Data source: <%# Eval("RenderingDataSource") %>
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 18%; background-color: #F5F5F5" title="A specific condition in the personalization rules">Conditions</td>
                    <td style="width: 12%; background-color: #F5F5F5" title="Total number of views">Views</td>
                    <td style="width: 12%; background-color: #F5F5F5" title="Total number of clicks">Clicks</td>
                    <td style="width: 12%; background-color: #F5F5F5" title=""><%# Eval("ViewsValueTitle") %></td>
                    <td style="width: 12%; background-color: #F5F5F5" title="Calculated effectiveness of one view">View effectiveness</td>
                    <td style="width: 12%; background-color: #F5F5F5" title=""><%# Eval("ClicksValueTitle") %></td>
                    <td style="width: 12%; background-color: #F5F5F5" title="Calculated effectiveness of one click">Click effectiveness</td>
                  </tr>
                  <asp:Repeater ID="innerRepeater" runat="server">
                    <ItemTemplate>
                      <tr>
                        <td style="width: 18%"><%# Eval("Condition") %></td>
                        <td style="width: 12%"><%# Eval("Views") %></td>
                        <td style="width: 12%"><%# Eval("Clicks") %></td>
                        <td style="width: 12%"><%# Eval("ViewsValue") %></td>
                        <td style="width: 12%"><%# Eval("ViewsEffectiveness") %></td>
                        <td style="width: 12%"><%# Eval("ClicksValue") %></td>
                        <td style="width: 12%"><%# Eval("ClicksEffectiveness") %></td>
                      </tr>
                    </ItemTemplate>
                  </asp:Repeater>
                </tbody>
              </table>
              <br />
            </ItemTemplate>
            <FooterTemplate></FooterTemplate>
          </asp:Repeater>
        </ContentTemplate>
      </asp:UpdatePanel>
    </div>
  </form>
</body>
</html>
