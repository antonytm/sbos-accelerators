﻿namespace Sitecore.SbosAccelerators.RelevantContentDetection.Data
{
    public interface IProfiledItemDistance : IProfiledItemCalculation
    {
        double Distance { get; }
        double Gravity { get; }
    }
}
