﻿namespace Sitecore.SbosAccelerators.HistoricalConditionalRenderings.Rules.Conditions
{
  # region Usings

  using System;
  using System.Collections.Generic;
  using System.Linq;
  using Sitecore.Analytics;
  using Sitecore.Analytics.Data.DataAccess;
  using Sitecore.Analytics.Data.DataAccess.DataSets;
  using Sitecore.Diagnostics;
  using Sitecore.Rules;
  using Sitecore.Rules.Conditions;

  # endregion Usings

  public class ProfileCondition<T> : HistoricalCondition<T> where T : RuleContext
  {
    # region Fields

    private string _profileKeyId;
    private string _value;
    private double _number;

    # endregion Fields

    # region Methods

    protected override bool Execute(T ruleContext)
    {
      Assert.ArgumentNotNull(ruleContext, "ruleContext");

      if (!double.TryParse(this.Value, out this._number)) return false;

      IEnumerable<VisitorDataSet.VisitsRow> visits = null;

      switch (this.GetPeriod().ToLower())
      {
        case ("days"):
          visits = this.GetVisitsByDays();
          break;

        case ("visits"):
          visits = this.GetVisits();
          break;
      }

      return visits != null && visits.Any(visit => this.CheckProfileKey(this.GetProfileKeyValue(visit)));
    }

    private bool CheckProfileKey(double profileKey)
    {
      switch (this.GetOperator())
      {
        case ConditionOperator.Equal: 
          return (Math.Abs(profileKey - this._number) < 0.001);

        case ConditionOperator.GreaterThanOrEqual:
          return (profileKey >= this._number);

        case ConditionOperator.GreaterThan:
          return (profileKey > this._number);

        case ConditionOperator.LessThanOrEqual:
          return (profileKey <= this._number);

        case ConditionOperator.LessThan:
          return (profileKey < this._number);

        case ConditionOperator.NotEqual:
          return (Math.Abs(profileKey - this._number) > 0.001);
      }

      return false;
    }

    protected ConditionOperator GetOperator()
    {
      if (!string.IsNullOrEmpty(this.OperatorId))
      {
        switch (this.OperatorId)
        {
          case "{066602E2-ED1D-44C2-A698-7ED27FD3A2CC}":
            return ConditionOperator.Equal;

          case "{814EF7D0-1639-44FD-AEEF-735B5AC14425}":
            return ConditionOperator.GreaterThanOrEqual;

          case "{B88CD556-082E-4385-BB76-E4D1B565F290}":
            return ConditionOperator.GreaterThan;

          case "{2E1FC840-5919-4C66-8182-A33A1039EDBF}":
            return ConditionOperator.LessThanOrEqual;

          case "{E362A3A4-E230-4A40-A7C4-FC42767E908F}":
            return ConditionOperator.LessThan;

          case "{3627ED99-F454-4B83-841A-A0194F0FB8B4}":
            return ConditionOperator.NotEqual;
        }
      }
      return ConditionOperator.Unknown;
    }

    private double GetProfileKeyValue(VisitorDataSet.VisitsRow visit)
    {
      if (string.IsNullOrEmpty(this.ProfileKeyId))
      {
        return 0.0;
      }

      var item = Tracker.DefinitionDatabase.GetItem(this.ProfileKeyId);

      if (item == null)
      {
        return 0.0;
      }

      var parent = item.Parent;

      if (parent == null)
      {
        return 0.0;
      }

      var profileKey = item.Name;
      var profileName = parent.Name;

      Tracker.Visitor.Load(new VisitorLoadOptions{Count = 1, VisitLoadOptions = VisitLoadOptions.Profiles, VisitId = visit.VisitId});
      var profileRow = visit.Profiles.FirstOrDefault(profile => profile.VisitId == visit.VisitId && string.Compare(profile.ProfileName, profileName, StringComparison.InvariantCultureIgnoreCase) == 0);

      return profileRow == null ? 0.0 : profileRow.GetValue(profileKey);
    }

    # endregion Methods

    # region Properties

    public string OperatorId { get; set; }

    public string Value
    {
      get { return (this._value ?? string.Empty); }
      set
      {
        Assert.ArgumentNotNull(value, "value");
        this._value = value;
      }
    }

    public string ProfileKeyId
    {
      get { return (this._profileKeyId ?? string.Empty); }
      set
      {
        Assert.ArgumentNotNull(value, "value");
        this._profileKeyId = value;
      }
    }

    # endregion Properties
  }
}