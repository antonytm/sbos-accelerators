﻿using Sitecore.Analytics.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace Sitecore.SbosAccelerators.Shared.Analytics.Data
{
  public static class TrackingFieldHelper
  {
    /// <summary>
    /// This method is an internal access correction for accessing TrackingField.GetTrackingField(Item item) method. Code was exported from 6.6 Update-6.
    /// </summary>
    /// <param name="item">The content item to get tracking field from.</param>
    public static TrackingField GetTrackingField(Item item)
    {
      Assert.ArgumentNotNull(item, "item");
      Field innerField = item.Fields["__Tracking"];
      if (innerField != null)
      {
        return new TrackingField(innerField);
      }

      return null;
    }
  }
}
