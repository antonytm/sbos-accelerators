﻿namespace Sitecore.SbosAccelerators.RelevantContentDetection.Data
{
    public interface IProfiledItemAssociation : IProfiledItemDistance
    {
        double Association { get; }
    }
}
