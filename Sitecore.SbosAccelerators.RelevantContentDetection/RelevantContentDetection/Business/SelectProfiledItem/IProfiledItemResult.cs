﻿using Sitecore.SbosAccelerators.RelevantContentDetection.Data;

namespace Sitecore.SbosAccelerators.RelevantContentDetection.Business.SelectProfiledItem
{
    public interface IProfiledItemResult
    {
        IProfiledItem ProfiledItem { get; }
        string Message { get; }
    }
}
