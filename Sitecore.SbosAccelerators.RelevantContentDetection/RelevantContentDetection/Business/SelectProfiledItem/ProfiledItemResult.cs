﻿using System;
using Sitecore.SbosAccelerators.RelevantContentDetection.Data;

namespace Sitecore.SbosAccelerators.RelevantContentDetection.Business.SelectProfiledItem
{
    public class ProfiledItemResult : Tuple<IProfiledItem, String>, IProfiledItemResult
    {
        public ProfiledItemResult(IProfiledItem item1, String item2)
            : base(item1, item2)
        {
        }

        public IProfiledItem ProfiledItem { get { return Item1; } }
        public string Message { get { return Item2; } }
    }

}
