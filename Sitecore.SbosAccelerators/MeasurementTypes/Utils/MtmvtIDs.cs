// -----------------------------------------------------------------------
// <copyright file="MtmvtIDs.cs" company="Sitecore A/S">
// Copyright � 2013 by Sitecore A/S
// </copyright>
// -----------------------------------------------------------------------

namespace Sitecore.SbosAccelerators.MeasurementTypes.Utils
{
  using System.Diagnostics.CodeAnalysis;
  using Sitecore.Data;

  /// <summary>
  /// Contains all system IDs.
  /// </summary>
  [SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:FieldsMustBePrivate", Justification = "Reviewed.")]
  public static class MtmvtIDs
  {
    /// <summary>
    /// 'Measurement Type' field on 'Multivariate Test Definition' template.
    /// </summary>
    public static readonly ID MeasurementTypeField = new ID("{5AB8BC69-D35B-45C9-9072-C3E6D19934E2}");

    /// <summary>
    /// 'Selected Goal' field on 'Multivariate Test Definition' template.
    /// </summary>
    public static readonly ID SelectedGoalField = new ID("{31EE42E6-C5F6-4377-AB92-3017E17936FF}");

    /// <summary>
    /// 'Variations SQL Query' field on 'Extended Multivariate Testing Settings' template.
    /// </summary>
    public static readonly ID VariationsSqlQueryField = new ID("{3C34B15C-B5E8-4753-AF4A-A5971600B28D}");

    /// <summary>
    /// Settings item.
    /// </summary>
    public static readonly ID Settings = new ID("{66A9910C-69EB-43E1-8581-4BDA9C43F7AE}");

    /// <summary>
    /// Category item under 'Page Events' folder.
    /// </summary>
    public static readonly ID XmvtCategoryItem = new ID("{3BF22204-16E0-4F79-9589-C5CF02CA74E9}");

    /// <summary>
    /// Analytics workflow 'Deployed' state.
    /// </summary>
    public static readonly ID AnalyticsDeployedWorkflowState = new ID("{EDCBB550-BED3-490F-82B8-7B2F14CCD26E}");

    /// <summary>
    /// '/sitecore/system/Modules/Measurement Types for Multivariate Testing/Measurement Types/EngagementValue' item.
    /// </summary>
    public static readonly ID EngagementValueItem = new ID("{6459DB74-9E54-447B-A8E5-15CC55D04F57}");

    /// <summary>
    /// '/sitecore/system/Modules/Measurement Types for Multivariate Testing/Measurement Types/SpecificGoal' item.
    /// </summary>
    public static readonly ID SpecificGoalItem = new ID("{3798195F-8551-4C6D-AABF-31DDCF0480E8}");

    /// <summary>
    /// '/sitecore/system/Modules/Measurement Types for Multivariate Testing/Measurement Types/ClickThroughRate' item.
    /// </summary>
    public static readonly ID ClickThroughRateItem = new ID("{D342AC14-FB1D-4774-A303-A8E2760B8052}");

    /// <summary>
    /// 'Display name' field on 'Measurement Type' template.
    /// </summary>
    public static readonly ID MeasurementTypeDisplayNameFieldId = new ID("{DE6C5490-CDAA-4452-83CC-20E3D429EBEF}");

    /// <summary>
    /// IDs of Measurement Types reports.
    /// </summary>
    public static class ReportIDs
    {
      /// <summary>
      /// 'By Component' report.
      /// </summary>
      public static readonly ID ByComponentEngagement = new ID("{8B4AA388-6E83-4D77-9AA2-861EFE3E3C6D}");

      /// <summary>
      /// 'By Component' report, conversion modification.
      /// </summary>
      public static readonly ID ByComponentConversions = new ID("{4600E932-3449-4F1A-8021-5FB2699E9BCF}");

      /// <summary>
      /// 'By Component' report, CTR modification.
      /// </summary>
      public static readonly ID ByComponentCtr = new ID("{33C01179-68DD-4BA0-97FB-1812BF37B43A}");

      /// <summary>
      /// 'By Combination' report.
      /// </summary>
      public static readonly ID ByCombination = new ID("{D949526B-C993-4EBA-B434-871EA0D3A864}");
      
      /// <summary>
      /// 'Combination Detail' report.
      /// </summary>
      public static readonly ID CombinationDetail = new ID("{94A9A300-BE14-4A40-8CA2-971D29B3C3C6}");

      /// <summary>
      /// 'Variant Detail' report.
      /// </summary>
      public static readonly ID VariantDetail = new ID("{B5BC4F40-9BA9-4E15-8D98-43B590F16F96}");

      /// <summary>
      /// 'Latest Tests' report.
      /// </summary>
      public static readonly ID LatestTests = new ID("{FEEE2827-4AC5-4E48-A689-D900489A8FE4}");
    }
  }
}
