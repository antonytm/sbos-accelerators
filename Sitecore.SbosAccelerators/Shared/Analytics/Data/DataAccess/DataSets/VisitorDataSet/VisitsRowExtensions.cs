﻿namespace Sitecore.SbosAccelerators.Shared.Analytics.Data.DataAccess.DataSets.VisitorDataSet
{
  using Sitecore.Web;

  public static class VisitsRowExtensions
  {
    /// <summary>
    /// This method is an internal access correction for accessing VisitsRow.UpdateSessionID() method. Code was exported from 6.6 Update-6.
    /// </summary>
    /// <param name="this">The instance of VisitsRow class.</param>
    public static void UpdateSessionID(this Sitecore.Analytics.Data.DataAccess.DataSets.VisitorDataSet.VisitsRow @this)
    {
      if (string.IsNullOrEmpty(@this.AspNetSessionId))
      {
        @this.AspNetSessionId = WebUtil.GetSessionID();
      }
    }
  }
}
