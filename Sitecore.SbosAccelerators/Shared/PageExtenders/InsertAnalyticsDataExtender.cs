﻿using System.Web;
using System.Web.UI.HtmlControls;
using Sitecore.Analytics;
using Sitecore.Diagnostics;
using Sitecore.Layouts.PageExtenders;

namespace Sitecore.SbosAccelerators.Shared.PageExtenders
{
  public class InsertAnalyticsDataExtender : PageExtender
  {
    public const string AnalyticsPageIdKey = @"SC_ANALYTICS_PAGE";

    public static string GetAnalyticsPageId()
    {
      var context = HttpContext.Current;
      Assert.IsNotNull(context, "HttpContext.Current");
      return context.Request.Form[AnalyticsPageIdKey];
    }

    public override void Insert()
    {
      if (Sitecore.Context.Site.Name == "shell" || Sitecore.Context.Site.Name == "modules_shell" || Sitecore.Context.Site.Name == "admin")
      {
        return;
      }

      this.InsertControl(new HtmlInputHidden { ID = AnalyticsPageIdKey, Value = Tracker.CurrentPage.PageId.ToString() });
    }
  }
}
