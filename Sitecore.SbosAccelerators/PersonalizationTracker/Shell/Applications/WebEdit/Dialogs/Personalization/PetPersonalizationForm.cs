namespace Sitecore.SbosAccelerators.PersonalizationTracker.Shell.Applications.WebEdit.Dialogs.Personalization
{
  using System;
  using System.Collections.Specialized;
  using System.Xml.Linq;
  using System.Linq;
  using Sitecore.Analytics.Data.Items;
  using Sitecore.Data;
  using Sitecore.Data.Fields;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.Extensions.XElementExtensions;
  using Sitecore.Globalization;
  using Sitecore.Layouts;
  using Sitecore.SecurityModel;
  using Sitecore.Shell.Applications.WebEdit.Dialogs.Personalization;
  using Sitecore.Web;
  using Sitecore.Web.UI.HtmlControls;
  using Sitecore.Web.UI.Sheer;

  public class PetPersonalizationForm : PersonalizationForm
  {
    protected Checkbox PetTracker;

    protected override void OnLoad(EventArgs e)
    {
      Assert.ArgumentNotNull(e, "e");
      base.OnLoad(e);

      XAttribute attribute = this.RulesSet.Attribute("pet_active");
      this.PetTracker.Checked = attribute != null && attribute.Value == "1";
    }

    protected new void ComponentPersonalizationClick()
    {
      if (!this.ComponentPersonalization.Checked && this.PersonalizeComponentActionExists())
      {
        var parameters = new NameValueCollection();
        Context.ClientPage.Start(this, "ShowConfirm", parameters);
      }
      else
      {
        SheerResponse.Eval("scTogglePersonalizeComponentSection()");
      }
    }

    protected override void OnOK(object sender, EventArgs args)
    {
      Assert.ArgumentNotNull(sender, "sender");
      Assert.ArgumentNotNull(args, "args");

      Log.Debug("[Sitecore.SbosAccelerators.PersonalizationTracker] Starting saving PET parameters.", this);

      XElement ruleSet = this.RulesSet;
      var petAttribute = ruleSet.Attribute("pet_active") ?? new XAttribute("pet_active", 1);
      petAttribute.Value = this.PetTracker.Checked ? "1" : "0";
      if (ruleSet.Attribute("pet_active") == null)
      {
        ruleSet.Add(petAttribute);
      }
      else
      {
        ruleSet.SetAttributeValue(petAttribute.Name, petAttribute.Value);
      }

      Item contextItem = this.ContextItem;
      contextItem.Fields.ReadAll();

      if (this.PetTracker.Checked)
      {
        using (new LanguageSwitcher("en"))
        {
          Database database = this.ContextItem.Database;
          Item petCategory = database.GetItem(PetIDs.PetPageEventCategory);

          Item categoryItem;
          if (petCategory.GetChildren().All(item => item.DisplayName != this.ContextItem.Name))
          {
            categoryItem = petCategory.Add(this.ContextItem.ID.ToString().Trim(new[] { '{', '}' }), new TemplateID(PageEventCategoryItem.TemplateID));
            using (new EditContext(categoryItem))
            {
              categoryItem[FieldIDs.DisplayName] = this.ContextItem.Name;
            }
          }
          else
          {
            categoryItem = petCategory.Children.SingleOrDefault(item => item.DisplayName == this.ContextItem.Name);
          }

          //if (categoryItem != null && !categoryItem.Children.Any(item => item.Name.Contains(this.RenderingDefition.ItemID)))
          if (categoryItem != null)
          {
            CreatePageEvents(categoryItem);
          }
        }
      }

      this.RulesSet = ruleSet;
      Log.Debug("[Sitecore.SbosAccelerators.PersonalizationTracker] Setting rule set to: " + this.RulesSet, this);

      SheerResponse.SetDialogValue(this.RulesSet.ToString());

      Log.Debug("[Sitecore.SbosAccelerators.PersonalizationTracker] Setting component's properties.", this);
      contextItem.Fields.ReadAll();
      LayoutField layout = contextItem.Fields[FieldIDs.LayoutField];
      LayoutDefinition layoutDefinition = LayoutDefinition.Parse(layout.Value);

      foreach (DeviceDefinition deviceDefinition in layoutDefinition.Devices)
      {
        if (deviceDefinition != null && deviceDefinition.Renderings != null)
        {
          foreach (RenderingDefinition renderingDefinition in deviceDefinition.Renderings)
          {
            if (renderingDefinition.UniqueId == this.RenderingDefition.UniqueId)
            {
              renderingDefinition.Rules = this.RulesSet;

              NameValueCollection parameters = StringUtil.ParseNameValueCollection(renderingDefinition.Parameters ?? "", '&', '=');

              parameters["PetActive"] = this.PetTracker.Checked ? "1" : "0";
              parameters["PetRenderingID"] = renderingDefinition.UniqueId;

              renderingDefinition.Parameters = StringUtil.NameValuesToString(parameters, "&").Replace(", ", "&");
            }
          }
        }
      }
      WebUtil.SetSessionValue("PEPesonalization", layoutDefinition.ToXml());

      Log.Debug("[Sitecore.SbosAccelerators.PersonalizationTracker] Set 'PEPesonalization' session value to: " + WebUtil.GetSessionValue("PEPesonalization"), this);

      SheerResponse.CloseWindow();
    }

    private void CreatePageEvents(Item categoryItem)
    {
      string viewItemName  = this.RenderingDefition.UniqueId.Trim(new[] { '{', '}' }) + "_{1}_{0}_View";
      string clickItemName = this.RenderingDefition.UniqueId.Trim(new[] { '{', '}' }) + "_{1}_{0}_Click";
      XElement ruleSet = this.RulesSet;

      using (new SecurityDisabler())
      {
        int nodeIndex = 0;
        foreach (XElement node in ruleSet.Nodes())
        {
          nodeIndex++;
          var xAttribute = node.Attribute("uid");
          if (xAttribute != null)
          {
            string viewName = string.Format(viewItemName, xAttribute.Value.Trim(new[] { '{', '}' }), nodeIndex.ToString("D2"));
            string clickName = string.Format(clickItemName, xAttribute.Value.Trim(new[] { '{', '}' }), nodeIndex.ToString("D2"));
            var attribute = node.Attribute("name");

            if (categoryItem.Children.All(item => item.Name != viewName))
            {
              Item viewItem = categoryItem.Add(viewName, new TemplateID(PageEventItem.TemplateID));
              PageEventItem viewPageEvent = PageEventItem.Create(viewItem);
              viewPageEvent.BeginEdit();
              viewPageEvent.IsSystem = true;
              viewPageEvent.EndEdit();

              if (attribute != null)
              {
                using (new EditContext(viewItem))
                {
                  viewItem.Fields[FieldIDs.WorkflowState].Value = PetIDs.AnalyticsDeployedWorkflowState.ToString();
                  viewItem.Fields[FieldIDs.DisplayName].Value = string.Format(viewItemName, attribute.Value.Trim(new[] { '{', '}' }), nodeIndex.ToString("D2"));
                  viewItem.Fields[PageEventItem.FieldIDs.Points].Value = "0";
                  viewItem.Fields[PageEventItem.FieldIDs.Category].Value = "PET";
                }
              }
            }

            if (categoryItem.Children.All(item => item.Name != clickName))
            {
              Item clickItem = categoryItem.Add(clickName, new TemplateID(PageEventItem.TemplateID));
              PageEventItem clickPageEvent = PageEventItem.Create(clickItem);
              clickPageEvent.BeginEdit();
              clickPageEvent.IsSystem = true;
              clickPageEvent.EndEdit();

              if (attribute != null)
              {
                using (new EditContext(clickItem))
                {
                  clickItem.Fields[FieldIDs.WorkflowState].Value = PetIDs.AnalyticsDeployedWorkflowState.ToString();
                  clickItem.Fields[FieldIDs.DisplayName].Value = string.Format(clickItemName, attribute.Value.Trim(new[] { '{', '}' }), nodeIndex.ToString("D2"));
                  clickItem.Fields[PageEventItem.FieldIDs.Points].Value = "0";
                  clickItem.Fields[PageEventItem.FieldIDs.Category].Value = "PET";
                }
              }
            }
          }
        }
      }
    }

    private bool PersonalizeComponentActionExists()
    {
      return this.RulesSet.Elements("rule").Any(rule => (GetActionById(rule, "{7B578B65-BD2F-4C7C-9A24-DAE3E98B4F23}") != null));
    }

    private static XElement GetActionById(XElement rule, string id)
    {
      Assert.ArgumentNotNull(rule, "rule");
      Assert.ArgumentNotNull(id, "id");
      XElement element = rule.Element("actions");
      if (element == null)
      {
        return null;
      }
      return element.Elements("action").FirstOrDefault(action => (action.GetAttributeValue("id") == id));
    }
  }
}
