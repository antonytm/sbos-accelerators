﻿namespace Sitecore.SbosAccelerators.HistoricalConditionalRenderings.Configuration
{
  #region Usings

  using System;
  using System.Collections.Specialized;
  using Sitecore.Configuration;
  using Sitecore.Data.Engines;
  using Sitecore.Data.Proxies;
  using Sitecore.Diagnostics;
  using Sitecore.Install.Files;
  using Sitecore.Install.Framework;
  using Sitecore.Install.Items;
  using Sitecore.Install.Utils;
  using Sitecore.IO;
  using Sitecore.SecurityModel;

  #endregion

  public class Installation : IPostStep
  {
    #region Public Methods

    public void Run(ITaskOutput output, NameValueCollection metaData)
    {
      var version = About.GetVersionNumber(false);
      var packagePathFor65 = FileUtil.MapPath("/Packages/Historical Conditional Renderings (6.5 - 7.0).zip");
      var packagePathFor71 = FileUtil.MapPath("/Packages/Historical Conditional Renderings (7.1).zip");

      try
      {
        switch (version)
        {
          case ("6.5.0"):
          case ("6.6.0"):
          case ("7.0"):
            {
              if (!System.IO.File.Exists(packagePathFor65)) return;

              this.InstallPackage(packagePathFor65);
              break;
            }

          case ("7.1"):
            {
              if (!System.IO.File.Exists(packagePathFor71)) return;

              this.InstallPackage(packagePathFor71);
              this.UpdateItems();
              break;
            }
        }
      }
      catch (Exception)
      {
        Log.Error("Installation post step action is failed", this);
      }
      finally
      {
        if (System.IO.File.Exists(packagePathFor65))
        {
          System.IO.File.Delete(packagePathFor65);
        }

        if (System.IO.File.Exists(packagePathFor71))
        {
          System.IO.File.Delete(packagePathFor71);
        }
      }
    }

    #endregion Public Methods

    #region Private Methods

    private void InstallPackage(string path)
    {
      var activeSite = Context.GetSiteName();

      Context.SetActiveSite("shell");

      using (new SecurityDisabler())
      {
        using (new ProxyDisabler())
        {
          using (new SyncOperationContext())
          {
            IProcessingContext context = new SimpleProcessingContext();

            IItemInstallerEvents itemInstallerEvents = new DefaultItemInstallerEvents(new BehaviourOptions(InstallMode.Overwrite, MergeMode.Undefined));
            context.AddAspect(itemInstallerEvents);

            IFileInstallerEvents fileInstallerEvents = new DefaultFileInstallerEvents(true);
            context.AddAspect(fileInstallerEvents);

            new Install.Installer().InstallPackage(MainUtil.MapPath(path), context);
          }
        }
      }

      Context.SetActiveSite(activeSite);
    }

    private void UpdateItems()
    {
      using (new SecurityDisabler())
      {
        this.UpdateConditionalRenderings();
        this.UpdateMarketingWorkflows();
      }
    }

    private void UpdateConditionalRenderings()
    {
      var master = Factory.GetDatabase("master");
      if (master == null) return;

      var conditionaRenderingsDegaultTag = master.GetItem("/sitecore/system/Settings/Rules/Conditional Renderings/Tags/Default");
      var tagsField = conditionaRenderingsDegaultTag.Fields["Tags"];

      if (tagsField == null) return;

      conditionaRenderingsDegaultTag.Editing.BeginEdit();

      if (tagsField.Value == string.Empty)
      {
        conditionaRenderingsDegaultTag.Editing.BeginEdit();
        tagsField.Value = "{04966997-E077-4954-8231-68E0B9E8DEA5}";
      }
      else
      {
        tagsField.Value += "|{04966997-E077-4954-8231-68E0B9E8DEA5}";
      }

      conditionaRenderingsDegaultTag.Editing.EndEdit();
    }

    private void UpdateMarketingWorkflows()
    {
      var master = Factory.GetDatabase("master");
      if (master == null) return;

      var marketingWorkflowsDefaultTag = master.GetItem("/sitecore/system/Settings/Rules/Marketing Workflows/Tags/Default");
      var tagsField = marketingWorkflowsDefaultTag.Fields["Tags"];

      if (tagsField == null) return;

      marketingWorkflowsDefaultTag.Editing.BeginEdit();

      if (tagsField.Value == string.Empty)
      {
        tagsField.Value = "{04966997-E077-4954-8231-68E0B9E8DEA5}";
      }
      else
      {
        tagsField.Value += "|{04966997-E077-4954-8231-68E0B9E8DEA5}";
      }

      marketingWorkflowsDefaultTag.Editing.EndEdit();
    }

    #endregion Private Methods
  }
}
