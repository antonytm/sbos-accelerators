﻿using System;
using Sitecore.SbosAccelerators.Shared.Analytics;
using Sitecore.SbosAccelerators.Shared.Analytics.Data.DataAccess.DataSets.VisitorDataSet;
using System.Web.UI;
using Sitecore.Analytics;
using Sitecore.Analytics.Configuration;
using Sitecore.Analytics.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Layouts;
using Sitecore.Pipelines;
using Sitecore.Sites;

namespace Sitecore.SbosAccelerators.AliasTracker.Analytics.Pipelines.StartAnalytics
{
  public class Init : Sitecore.Analytics.Pipelines.StartAnalytics.Init
  {
    // method was exported from original Init class of 6.6 Update-6
    public override void Process(PipelineArgs args)
    {
      Assert.ArgumentNotNull(args, "args");

      if (AnalyticsSettings.Enabled)
      {
        SiteContext site = Context.Site;
        if (((site != null) && site.EnableAnalytics) && (site.DisplayMode == DisplayMode.Normal))
        {
          PageContext context2 = Context.Page;
          if (context2 != null)
          {
            Page page = context2.Page;
            var disableAnalytics = IgnoreCurrentItem();
            if ((page != null) && !disableAnalytics)
            {
              Tracker.StartTracking();
              if (Tracker.IsActive)
              {
                page.LoadComplete += this.Page_LoadComplete;
              }
            }
          }
        }
      }
    }
    
    // method was exported from original Init class of 6.6 Update-6
    private void Page_LoadComplete(object sender, EventArgs e)
    {
      Assert.ArgumentNotNull(sender, "sender");
      Assert.ArgumentNotNull(e, "e");
      if (!Context.Items.Contains("Page_LoadComplete_Lock"))
      {
        Context.Items["Page_LoadComplete_Lock"] = new object();
        if (Tracker.IsActive)
        {
          Tracker.CurrentVisit.UpdateSessionID();
        }
      }
    }

    // method was exported from original Init class of 6.6 Update-6
    // it was modified to check also alias' settings
    protected virtual bool IgnoreCurrentItem()
    {
      var aliasItem = AliasHelper.GetCurrentAliasItem();
      Item item = Context.Item;
      if(item == null) return false;
      return (IgnoreItem(aliasItem ?? item) ?? false) || (IgnoreItem(item) ?? false);
    }

    // this method was exported from base.IgnoreCurrentItem() method. Code was exported from 6.6 Update-6.
    private bool? IgnoreItem(Item item)
    {
      TrackingField field = TrackerHelper.FindTrackingField(item);
      if (field != null)
      {
        return field.Ignore;
      }

      return null;
    }
  }
}
