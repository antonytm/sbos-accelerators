﻿namespace Sitecore.SbosAccelerators.HistoricalConditionalRenderings.Rules.Conditions
{
  # region Usings

  using System;
  using System.Collections.Generic;
  using System.Linq;
  using Sitecore.Analytics;
  using Sitecore.Analytics.Data.DataAccess;
  using Sitecore.Analytics.Data.DataAccess.DataSets;
  using Sitecore.Diagnostics;
  using Sitecore.Rules;

  # endregion Usings

  public class HasPatternCondition<T> : HistoricalCondition<T> where T : RuleContext
  {
    # region Methods

    protected override bool Execute(T ruleContext)
    {
      Assert.ArgumentNotNull(ruleContext, "ruleContext");

      IEnumerable<VisitorDataSet.VisitsRow> visits = null;

      switch (this.GetPeriod().ToLower())
      {
        case ("days"):
          visits = this.GetVisitsByDays();
          break;

        case ("visits"):
          visits = this.GetVisits();
          break;
      }

      if (visits == null) return false;

      foreach (var visit in visits)
      {
        Tracker.Visitor.Load(new VisitorLoadOptions { Count = 1, VisitLoadOptions = VisitLoadOptions.Profiles, VisitId = visit.VisitId });

        if (visit.Profiles.Where(profilesRow => profilesRow.VisitId == visit.VisitId && string.Compare(profilesRow.ProfileName, this.ProfileName, StringComparison.InvariantCultureIgnoreCase) == 0)
           .Any(profilesRow => string.Compare(profilesRow.PatternLabel, this.PatternName, StringComparison.InvariantCultureIgnoreCase) == 0)) return true;
      }

      return false;
    }

    # endregion Methods

    # region Properties

    public string PatternName { get; set; }
    public string ProfileName { get; set; }

    # endregion Properties
  }
}
