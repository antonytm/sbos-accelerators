﻿using Sitecore.SbosAccelerators.RelevantContentDetection.Data;

namespace Sitecore.SbosAccelerators.RelevantContentDetection.Business.SelectProfiledItem
{
    public interface ISelectFuzzyProfiledItem : ISelectProfiledItem
    {
        IProfiledItemResult GetFuzzyProfiledItem(IProfiledItemList list);
    }
}
