namespace Sitecore.SbosAccelerators.Shared.Presentation
{
  using System;
  using System.Collections.Generic;
  using Sitecore.Diagnostics;
  using Sitecore.Presentation;
  using Sitecore.Web.UI.WebControls;

  public class XslControlUriParser : ControlUriParser
  {
    private readonly Dictionary<string, string> parameters;
    private readonly string xslPath;

    public XslControlUriParser(string controlUri, Dictionary<string, string> parameters)
    {
      Assert.ArgumentNotNull(controlUri, "controlUri");
      Assert.ArgumentNotNull(parameters, "parameters");
      const string Prefix = "xsl://";
      if ((controlUri.Length <= Prefix.Length) || !controlUri.StartsWith(Prefix, StringComparison.InvariantCulture))
      {
        throw new ArgumentException("Invalid control uri passed to XslControlUriParser (must start with 'xsl:' and contain a path to an XSLT file): " + controlUri);
      }
      this.xslPath = controlUri.Substring(Prefix.Length);
      this.parameters = parameters;
    }

    public override IWebControl GetControl()
    {
      var file = new Web.UI.WebControls.XslFile
      {
        Path = this.xslPath
      };
      file.SetParameters(this.parameters);
      return file;
    }
  }
}
