﻿namespace Sitecore.SbosAccelerators.HistoricalConditionalRenderings.Rules.Conditions
{
  # region Usings

  using System;
  using System.Collections.Generic;
  using System.Linq;
  using Sitecore.Analytics.Data.DataAccess;
  using Sitecore.Analytics.Data.DataAccess.DataSets;
  using Sitecore.Rules;
  using Sitecore.Rules.Conditions;

  # endregion Usings
  
  public abstract class HistoricalCondition<T> : WhenCondition<T> where T: RuleContext
  {
    # region Fields

    protected DateTime StartDate;

    # endregion Fields

    # region Methods

    protected string GetPeriod()
    {
      switch (this.Period)
      {
        case("{FB56B0B4-13B1-4479-BEEC-DFCB8A26E6B0}"):
          return "visits";
      }

      return "days";
    }

    protected IEnumerable<VisitorDataSet.VisitsRow> GetVisits()
    {
      var visitor = Analytics.Tracker.Visitor;
      var startIndex = (visitor.VisitCount <= this.Count) ? 1 : visitor.VisitCount - this.Count + 1;
      
      var visits = visitor.GetVisits(new VisitorLoadOptions
        {
          Count = visitor.VisitCount,
          VisitLoadOptions = VisitLoadOptions.All,
          Options = VisitorOptions.All,
          NotOlderThan = DateTime.MinValue,
          Start = startIndex
        });

      return visits.OrderByDescending(visit => visit.StartDateTime);
    }

    protected IEnumerable<VisitorDataSet.VisitsRow> GetVisitsByDays()
    {
      this.StartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(-(this.Count - 1));
      var visitor = Analytics.Tracker.Visitor;
      
      var visits = visitor.GetVisits(new VisitorLoadOptions
        {
          Count = visitor.VisitCount, 
          VisitLoadOptions = VisitLoadOptions.All, 
          Options = VisitorOptions.All, 
          NotOlderThan = this.StartDate, 
          Start = 1
        });

      return visits.Where(visit => visit.EndDateTime >= this.StartDate).OrderByDescending(visit => visit.StartDateTime);
    }

    # endregion Methods

    # region Properties

    public string Period { get; set; }
    public int Count { get; set; }

    # endregion Properties
  }
}
