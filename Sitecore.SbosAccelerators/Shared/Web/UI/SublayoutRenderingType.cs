﻿namespace Sitecore.SbosAccelerators.Shared.Web.UI
{
  using System.Collections.Specialized;
  using System.Web.UI;
  using Sitecore.Reflection;

  public class SublayoutRenderingType : Sitecore.Web.UI.SublayoutRenderingType
  {
    public override Control GetControl(NameValueCollection parameters, bool assert)
    {
      var sublayout = new Sitecore.SbosAccelerators.Shared.Web.UI.WebControls.Sublayout();
      foreach (string str in parameters.Keys)
      {
        string str2 = parameters[str];
        ReflectionUtil.SetProperty(sublayout, str, str2);
      }

      return sublayout;
    }
  }
}
