﻿namespace Sitecore.SbosAccelerators.HistoricalConditionalRenderings.Rules.RuleMacros
{
  # region Usings

  using System.Xml.Linq;
  using Sitecore.Diagnostics;
  using Sitecore.Rules.RuleMacros;
  using Sitecore.Shell.Applications.Dialogs.ItemLister;
  using Sitecore.Text;
  using Sitecore.Web.UI.Sheer;

  # endregion Usings

  public class PeriodMacro : IRuleMacro
  {
    # region Methods

    public void Execute(XElement element, string name, UrlString parameters, string value)
    {
      Assert.ArgumentNotNull(element, "element");
      Assert.ArgumentNotNull(name, "name");
      Assert.ArgumentNotNull(parameters, "parameters");
      Assert.ArgumentNotNull(value, "value");
      
      var options = new SelectItemOptions();
      
      if (!string.IsNullOrEmpty(value))
      {
        var item = Client.ContentDatabase.GetItem(value);
        
        if (item != null)
        {
          options.SelectedItem = item;
        }
      }

      options.Root = Client.ContentDatabase.GetItem(Client.ContentDatabase.GetItem("/sitecore/system/Settings/Rules/Common/Macros/Period") != null ? "/sitecore/system/Settings/Rules/Common/Macros/Period" : "/sitecore/system/Settings/Rules/Definitions/Macros/Period");
      options.Title = "Select Period";
      options.Text = "Select the period type to use in this rule.";
      options.Icon = "applications/32x32/media_stop.png";
      options.ShowRoot = false;

      SheerResponse.ShowModalDialog(options.ToUrlString().ToString(), true);
    }

    # endregion Usings
  }
}
