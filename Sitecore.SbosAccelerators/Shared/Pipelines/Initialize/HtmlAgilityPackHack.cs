﻿namespace Sitecore.SbosAccelerators.Shared.Pipelines.Initialize
{
  using System;
  using System.Reflection;
  using Sitecore.Diagnostics;
  using Sitecore.Pipelines;
  using Sitecore.StringExtensions;

  public class HtmlAgilityPackHack
  {
    public void Process(PipelineArgs e)
    {
      AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
      {
        Log.Info("[SBOS] Trying to load assembly with name '{0}'.".FormatWith(args.Name), this);

        string resourceName = "Sitecore.SbosAccelerators.References." + new AssemblyName(args.Name).Name + ".dll";

        using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
        {
          if (stream == null)
          {
            Log.Debug("[SBOS] Stream was null.", this);
            return null;
          }

          var assemblyData = new byte[stream.Length];
          stream.Read(assemblyData, 0, assemblyData.Length);
          return Assembly.Load(assemblyData);
        }
      };
    }
  }
}
