﻿using System.Collections.Generic;

namespace Sitecore.SbosAccelerators.RelevantContentDetection.Data
{
    public interface IProfiledItemList : IList<IProfiledItem>
    {
    }
}
