﻿namespace Sitecore.SbosAccelerators.PersonalizationTracker
{
  using Sitecore.Data;

  public static class PetIDs
  {
    public static ID PetPageEventCategory = ID.Parse("6DDC0375-4967-4B19-A4F8-E8DDD172DBDC");
    public static ID AnalyticsDeployedWorkflowState = ID.Parse("EDCBB550-BED3-490F-82B8-7B2F14CCD26E");
  }
}
