﻿namespace Sitecore.SbosAccelerators.RelevantContentDetection.Data
{
    public interface IProfiledItemWeight : IProfiledItemDistance
    {
        double Weight { get; }
    }
}
