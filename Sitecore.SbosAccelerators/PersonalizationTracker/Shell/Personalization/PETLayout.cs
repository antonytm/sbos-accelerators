﻿namespace Sitecore.SbosAccelerators.PersonalizationTracker.Shell.Personalization
{
  using System;
  using System.Collections.Generic;
  using System.Data;
  using System.Data.SqlClient;
  using System.IO;
  using System.Linq;
  using System.Web.UI;
  using System.Web.UI.WebControls;
  using System.Xml.Linq;
  using Sitecore.Analytics;
  using Sitecore.Analytics.Configuration;
  using Sitecore.Analytics.Data.DataAccess.DataAdapters;
  using Sitecore.Analytics.Data.Items;
  using Sitecore.Data;
  using Sitecore.Data.Fields;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.Globalization;
  using Sitecore.Layouts;
  using Sitecore.SbosAccelerators.PersonalizationTracker.Caching;
  using Sitecore.Shell.Applications.Rules;
  using Sitecore.Web;

  public class PETLayout : System.Web.UI.Page
  {
    public class RenderingRecord
    {
      public class ValueRecord
      {
        public string Condition { get; set; }
        public string Views { get; set; }
        public string ViewsValue { get; set; }
        public string ViewsEffectiveness { get; set; }
        public string Clicks { get; set; }
        public string ClicksValue { get; set; }
        public string ClicksEffectiveness { get; set; }
      }

      public string RenderingName { get; set; }
      public string RenderingDataSource { get; set; }
      public List<ValueRecord> Values { get; set; }
      public string ViewsValueTitle { get; set; }
      public string ClicksValueTitle { get; set; }

      public RenderingRecord()
      {
        this.Values = new List<ValueRecord>();
      }
    }

    protected Repeater outerRepeater;
    protected DropDownList goalDropdown;
    protected RadioButtonList modeRadiogroup;
    protected Panel goalDropdownPanel;
    protected CheckBox showHistoricalCheckBox;
    protected UpdatePanel updatePanel;
    protected ScriptManager ScriptManager1;

    private readonly SqlConnection connection = new SqlConnection(DataAdapterManager.ConnectionStrings.Analytics);

    private static volatile object syncRoot = new object();

    protected void Page_Load(object sender, EventArgs e)
    {
      ScriptManager1.RegisterAsyncPostBackControl(outerRepeater);

      if (!Page.IsPostBack)
      {
        BindData();

        // filling goal list
        Database database = Sitecore.Context.ContentDatabase;
        Item goalsRoot = database.GetItem(AnalyticsIds.GoalsRoot);
        goalDropdown.Items.Clear();
        goalDropdown.Items.Add("None");
        foreach (Item child in goalsRoot.Axes.GetDescendants().Where(descendant => descendant.TemplateID == AnalyticsIds.PageEvent || descendant.TemplateID == AnalyticsIds.Goal))
        {
          goalDropdown.Items.Add(child.Name);
        }
        goalDropdown.SelectedIndex = 0;
      }
    }

    public void BindData()
    {
      using (new LanguageSwitcher("en"))
      {
        Database master = Sitecore.Context.ContentDatabase;

        SqlCommand gridCommand;
        DataSet gridDataSet;
        
        #region Queries

        const string renderingsQuery = @";with ViewData as
(
select
 row_number() over(order by rn) as rownumber,
 pe.VisitId,
 pe.Data,
 pe.DataKey,
 pe.Text,
 pe.rn,
 count(rn) over(partition by DataKey, Text) as Cnt
from
 PageEvents as pe
where 
 pe.ItemId = @Id 
 and pe.Text = 'View'
),
ClickData as
(
select
 row_number() over(order by rn) as rownumber,
 pe.VisitId,
 pe.Data,
 pe.DataKey,
 pe.Text,
 pe.rn,
 count(rn) over(partition by DataKey, Text) as Cnt
from
 PageEvents as pe
where 
 pe.ItemId = @Id 
 and pe.Text = 'Click'
 and pe.Data like '%|%'
)
-------
select 
 ViewStats.Data as RenderingName,
 ViewStats.DataKey as Conditions,
 max(ViewStats.Views) as Views,
 max(ClickStats.Clicks) as Clicks,
 sum(ViewStats.SumValue) as ValueViewContributed,
 cast(sum(cast(ViewStats.SumValue as decimal(25,13)) / cast(Views as decimal(25,13))) as decimal(36,2)) as [ViewEffectiveness],
 sum(ClickStats.SumValue) as ValueClickContributed,
 cast(sum(cast(ClickStats.SumValue as decimal(25,13)) / cast(Clicks as decimal(25,13))) as decimal(36,2)) as [ClickEffectiveness]
from
(
 select 
 VisitId,
 Data,
 DataKey,
 Text,
 max(Cnt) as Views,
 sum(PageEventsValue) as SumValue
 from
 (
 select 
 isnull(d1.VisitId, d2.VisitId) as VisitId,
 isnull(d1.Data, d2.Data) as Data,
 isnull(d1.DataKey, d2.DataKey) as DataKey,
 isnull(d1.Text, d2.Text) as Text,
 isnull(d1.rn, 9223372036854775807) as rn1,
 isnull(d2.rn, 9223372036854775807) as rn2,
 (
 select 
 isnull(sum(Value),0) 
 from 
 PageEvents as pe, 
 PageEventDefinitions as ped 
 where 
 pe.PageEventDefinitionId = ped.PageEventDefinitionId and
 pe.VisitId = d1.VisitId and
 pe.rn > isnull(d1.rn, 9223372036854775807) and
 pe.rn < isnull(d2.rn, 9223372036854775807)
 ) as PageEventsValue,
 isnull(d1.Cnt, d2.Cnt) as cnt
 from 
 ViewData d1 full join ViewData d2 on d1.rownumber = d2.rownumber - 1
 ) as Data
 group by
 VisitId,
 Data,
 DataKey,
 Text
) as ViewStats
full join
(
 select 
 VisitId,
 Data,
 DataKey,
 Text,
 max(Cnt) as Clicks,
 sum(PageEventsValue) as SumValue
 from
 (
 select 
 isnull(d1.VisitId, d2.VisitId) as VisitId,
 isnull(d1.Data, d2.Data) as Data,
 isnull(d1.DataKey, d2.DataKey) as DataKey,
 isnull(d1.Text, d2.Text) as Text,
 isnull(d1.rn, 9223372036854775807) as rn1,
 isnull(d2.rn, 9223372036854775807) as rn2,
 (
 select 
 isnull(sum(Value),0) 
 from 
 PageEvents as pe, 
 PageEventDefinitions as ped 
 where 
 pe.PageEventDefinitionId = ped.PageEventDefinitionId and
 pe.VisitId = d1.VisitId and
 pe.rn > isnull(d1.rn, 9223372036854775807) and
 pe.rn < isnull(d2.rn, 9223372036854775807)
 ) as PageEventsValue,
 isnull(d1.Cnt, d2.Cnt) as cnt
 from 
 ClickData d1 full join ClickData d2 on d1.rownumber = d2.rownumber - 1
 ) as Data
 group by
 VisitId,
 Data,
 DataKey,
 Text
) as ClickStats
on 
 ViewStats.VisitId = ClickStats.VisitId and
 ViewStats.Data = ClickStats.Data and
 ViewStats.DataKey = ClickStats.DataKey
group by 
 ViewStats.Data,
 ViewStats.DataKey";

        const string goalsQuery = @";with ViewData as
(
select
 row_number() over(order by rn) as rownumber,
 pe.VisitId,
 pe.Data,
 pe.DataKey,
 pe.Text,
 pe.rn,
 count(rn) over(partition by DataKey, Text) as Cnt
from
 PageEvents as pe
where 
 pe.ItemId = @Id 
 and pe.Text = 'View'
),
ClickData as
(
select
 row_number() over(order by rn) as rownumber,
 pe.VisitId,
 pe.Data,
 pe.DataKey,
 pe.Text,
 pe.rn,
 count(rn) over(partition by DataKey, Text) as Cnt
from
 PageEvents as pe
where 
 pe.ItemId = @Id 
 and pe.Text = 'Click'
)
-------
select 
 ViewStats.Data as RenderingName,
 ViewStats.DataKey as Conditions,
 max(ViewStats.Views) as Views,
 max(ClickStats.Clicks) as Clicks,
 sum(ViewStats.SumValue) as ValueViewContributed,
 cast(sum(cast(ViewStats.SumValue as decimal(25,13)) / cast(Views as decimal(25,13))) as decimal(36,2)) as [ViewEffectiveness],
 sum(ClickStats.SumValue) as ValueClickContributed,
 cast(sum(cast(ClickStats.SumValue as decimal(25,13)) / cast(Clicks as decimal(25,13))) as decimal(36,2)) as [ClickEffectiveness]
from
(
 select 
 VisitId,
 Data,
 DataKey,
 Text,
 max(Cnt) as Views,
 sum(PageEventsValue) as SumValue
 from
 (
 select 
 isnull(d1.VisitId, d2.VisitId) as VisitId,
 isnull(d1.Data, d2.Data) as Data,
 isnull(d1.DataKey, d2.DataKey) as DataKey,
 isnull(d1.Text, d2.Text) as Text,
 isnull(d1.rn, 9223372036854775807) as rn1,
 isnull(d2.rn, 9223372036854775807) as rn2,
 (
 select 
 count(Value) 
 from 
 PageEvents as pe, 
 PageEventDefinitions as ped 
 where 
 pe.PageEventDefinitionId = ped.PageEventDefinitionId and
 pe.VisitId = d1.VisitId and
 pe.rn > isnull(d1.rn, 9223372036854775807) and
 pe.rn < isnull(d2.rn, 9223372036854775807) and
 ped.Name = @Name
 ) as PageEventsValue,
 isnull(d1.Cnt, d2.Cnt) as cnt
 from 
 ViewData d1 full join ViewData d2 on d1.rownumber = d2.rownumber - 1
 ) as Data
 group by
 VisitId,
 Data,
 DataKey,
 Text
) as ViewStats
full join
(
 select 
 VisitId,
 Data,
 DataKey,
 Text,
 max(Cnt) as Clicks,
 sum(PageEventsValue) as SumValue
 from
 (
 select 
 isnull(d1.VisitId, d2.VisitId) as VisitId,
 isnull(d1.Data, d2.Data) as Data,
 isnull(d1.DataKey, d2.DataKey) as DataKey,
 isnull(d1.Text, d2.Text) as Text,
 isnull(d1.rn, 9223372036854775807) as rn1,
 isnull(d2.rn, 9223372036854775807) as rn2,
 (
 select 
 count(Value)
 from 
 PageEvents as pe, 
 PageEventDefinitions as ped 
 where 
 pe.PageEventDefinitionId = ped.PageEventDefinitionId and
 pe.VisitId = d1.VisitId and
 pe.rn > isnull(d1.rn, 9223372036854775807) and
 pe.rn < isnull(d2.rn, 9223372036854775807) and
 ped.Name = @Name
 ) as PageEventsValue,
 isnull(d1.Cnt, d2.Cnt) as cnt
 from 
 ClickData d1 full join ClickData d2 on d1.rownumber = d2.rownumber - 1
 ) as Data
 group by
 VisitId,
 Data,
 DataKey,
 Text
) as ClickStats
on 
 ViewStats.VisitId = ClickStats.VisitId and
 ViewStats.Data = ClickStats.Data and
 ViewStats.DataKey = ClickStats.DataKey
group by 
 ViewStats.Data,
 ViewStats.DataKey";

        #endregion

        if (this.modeRadiogroup.SelectedIndex == 0)
        {
          this.goalDropdown.SelectedIndex = 0;

          gridCommand = new SqlCommand(renderingsQuery, this.connection);
          gridCommand.Parameters.Add(new SqlParameter("Id", WebUtil.GetQueryString("id")));

          gridDataSet = ProcessCachedDataSet(string.Empty, gridCommand);
        }
        else
        {

          gridCommand = new SqlCommand(goalsQuery, this.connection);
          gridCommand.Parameters.Add(new SqlParameter("Id", WebUtil.GetQueryString("id")));
          gridCommand.Parameters.Add(new SqlParameter("Name", this.goalDropdown.SelectedValue));

          string points = "1.0";

          if (this.goalDropdown.SelectedValue != "None")
          {
            try
            {
              Item goalItem = master.GetItem(AnalyticsIds.GoalsRoot).Axes.GetChild(this.goalDropdown.SelectedValue) ?? master.GetItem(AnalyticsIds.PageEventRoot).Axes.GetChild(this.goalDropdown.SelectedValue);

              points = goalItem[PageEventItem.FieldIDs.Points];

              if (points == "0" || points == "0.0")
              {
                points = "1.0";
              }
            }
            catch (Exception ex)
            {
              Log.Error("An error happeneed while trying to get points for goal " + this.goalDropdown.SelectedValue, ex, this);
            }
          }

          gridCommand.Parameters.Add(new SqlParameter("Multiplier", SqlDbType.Float)
          {
            Value = points
          });

          gridDataSet = ProcessCachedDataSet(this.goalDropdown.SelectedValue, gridCommand);
        }

        try
        {
          var dataSource = new List<RenderingRecord>();
          foreach (DataTable table in gridDataSet.Tables)
          {
            RenderingRecord rendering = null;
            foreach (DataRow row in table.DefaultView.ToTable().Rows)
            {
              if (rendering == null)
              {
                rendering = new RenderingRecord
                {
                  RenderingName = row["RenderingName"].ToString()
                };
              }

              if (!string.IsNullOrEmpty(rendering.RenderingName))
              {
              if (!rendering.RenderingName.Substring(rendering.RenderingName.IndexOf('|')).Equals(row["RenderingName"].ToString().Substring(row["RenderingName"].ToString().IndexOf('|')), StringComparison.OrdinalIgnoreCase))
              {
                this.PrepareRenderingRecord(rendering);
                dataSource.Add(rendering);
                rendering = new RenderingRecord
                {
                  RenderingName = row["RenderingName"].ToString()
                };
              }

              var valueRecord = new RenderingRecord.ValueRecord();

              if (this.modeRadiogroup.SelectedIndex == 0)
              {
                rendering.ViewsValueTitle = "Value (view contributed)";
                rendering.ClicksValueTitle = "Value (click contributed)";
              }
              else
              {
                rendering.ViewsValueTitle = string.Format("'{0}' conversions (view contributed)", this.goalDropdown.SelectedValue);
                rendering.ClicksValueTitle = string.Format("'{0}' conversions (click contributed)", this.goalDropdown.SelectedValue);
              }

              valueRecord.Condition = FormatNulls(row["Conditions"].ToString());
              valueRecord.Views = FormatNulls(row["Views"].ToString());
              valueRecord.Clicks = FormatNulls(row["Clicks"].ToString());
              valueRecord.ViewsValue = FormatNulls(row["ValueViewContributed"].ToString());
              valueRecord.ViewsEffectiveness = FormatNulls(row["ViewEffectiveness"].ToString());
              valueRecord.ClicksValue = FormatNulls(row["ValueClickContributed"].ToString());
              valueRecord.ClicksEffectiveness = FormatNulls(row["ClickEffectiveness"].ToString());


              int index = rendering.Values.FindIndex(value => value.Condition.Equals(valueRecord.Condition, StringComparison.OrdinalIgnoreCase));

              if (index >= 0)
              {
                if (rendering.Values[index].Views.Equals("&mdash;", StringComparison.OrdinalIgnoreCase) && rendering.Values[index].Clicks.Equals("&mdash;", StringComparison.OrdinalIgnoreCase))
                {
                  rendering.Values[index] = valueRecord;
                }
              }
              else
              {
                rendering.Values.Add(valueRecord);
              }
            }
            }

            if (rendering != null && !string.IsNullOrEmpty(rendering.RenderingName))
            {
              this.PrepareRenderingRecord(rendering);
              dataSource.Add(rendering);
            }
          }

          this.outerRepeater.DataSource = dataSource.OrderBy(r => r.RenderingName);
          this.outerRepeater.DataBind();
        }
        catch (Exception exception)
        {
          Log.Error("[Sitecore.SbosAccelerators.PersonalizationTracker] An error happened during DataBinding.", exception, this);
        }
      }
    }

    private void PrepareRenderingRecord(RenderingRecord rendering)
    {
      if (!string.IsNullOrEmpty(rendering.RenderingName))
      {
      int index = rendering.RenderingName.LastIndexOf('|') + 2;
      string renderingId = rendering.RenderingName.Substring(index > 0 ? index : 0);

      Database master = Sitecore.Context.ContentDatabase;
      string itemId = WebUtil.GetQueryString("id");

      Item contextItem = master.GetItem(new ID(itemId));
      if (contextItem == null)
      {
        return;
      }

      contextItem.Fields.ReadAll();
      LayoutField layout = contextItem.Fields[FieldIDs.LayoutField];
      LayoutDefinition layoutDefinition = LayoutDefinition.Parse(layout.Value);

      rendering.RenderingDataSource = "unknown";

      foreach (DeviceDefinition device in layoutDefinition.Devices)
      {
        foreach (RenderingDefinition renderingDefinition in device.Renderings.Cast<RenderingDefinition>())
        {
          if (renderingDefinition.UniqueId.Equals(renderingId, StringComparison.OrdinalIgnoreCase))
          {
            string ds = renderingDefinition.Datasource;
            if (!string.IsNullOrEmpty(ds))
            {
              Item dsItem = master.GetItem(ds);
              if (dsItem != null)
              {
                string openCElink = string.Format("/sitecore/shell/Applications/Content Editor?id={0}&vs=1&la=en&sc_content=master&fo={0}", dsItem.ID);
                rendering.RenderingDataSource = string.Format("<a href=\"{0}\" target=\"blank\">{1}</>", openCElink, renderingDefinition.Datasource);
              }
            }

            for (int i = 0; i < rendering.Values.Count; i++)
            {
              foreach (XElement rule in renderingDefinition.Rules.Elements("rule"))
              {
                string conditionName = rendering.Values[i].Condition.Substring(rendering.Values[i].Condition.IndexOf('_') + 1);
                var xAttribute = rule.Attribute("name");
                if (xAttribute != null && conditionName.Equals(xAttribute.Value, StringComparison.OrdinalIgnoreCase))
                {
                  HtmlTextWriter output = new HtmlTextWriter(new StringWriter());
                  RulesRenderer renderer2 = new RulesRenderer("<ruleset>" + rule + "</ruleset>")
                  {
                    SkipActions = true
                  };
                  renderer2.Render(output);
                  rendering.Values[i].Condition = output.InnerWriter.ToString();
                }
              }
            }

            return;
          }
        }
      }
    }
    }

    private string FormatNulls(string input)
    {
      return input == "0" || input == "0.00" || string.IsNullOrEmpty(input) ? "&mdash;" : input;
    }

    private DataSet ProcessCachedDataSet(string key, SqlCommand gridCommand)
    {
      string itemId = WebUtil.GetQueryString("id");
      if (!DataSetCache.PerItemCache.ContainsKey(itemId))
      {
        DataSetCache.PerItemCache.Add(itemId, new Dictionary<string, DataSetCache.CachedDataSet>());
      }

      DataSet gridDataSet;
      lock (syncRoot)
      {
        if (DataSetCache.PerItemCache[itemId].ContainsKey(key))
        {
          if (DateTime.Now - DataSetCache.PerItemCache[itemId][key].CreatedAt < TimeSpan.FromMilliseconds(AnalyticsSettings.TrackerChanges.FlushInterval))
          {
            gridDataSet = DataSetCache.PerItemCache[itemId][key].DataSet;
          }
          else
          {
            gridDataSet = new DataSet();
            var dataAdapter = new SqlDataAdapter(gridCommand);
            dataAdapter.Fill(gridDataSet);
            this.PostProcessDataSet(gridDataSet);

            DataSetCache.PerItemCache[itemId].Remove(key);
          }
        }
        else
        {
          gridDataSet = new DataSet();
          var dataAdapter = new SqlDataAdapter(gridCommand);
          dataAdapter.Fill(gridDataSet);
          this.PostProcessDataSet(gridDataSet);

          DataSetCache.PerItemCache[itemId].Add(key, new DataSetCache.CachedDataSet(DateTime.Now, gridDataSet));
        }
      }

      return gridDataSet;
    }

    private void PostProcessDataSet(DataSet gridDataSet)
    {
      string itemId = WebUtil.GetQueryString("id");
      Database master = Sitecore.Context.ContentDatabase;

      // Adding conditions for which no analytic data is present (костыль).
      var renderingConditions = new Dictionary<string, List<string>>();
      var renderingsToRemove = new List<ID>();
      if (itemId != "00000000-0000-0000-0000-000000000000")
      {
        Item item = master.GetItem(new ID(itemId));
        item.Fields.ReadAll();
        LayoutField layout = item.Fields[FieldIDs.LayoutField];
        if (!string.IsNullOrEmpty(layout.Value))
        {
          LayoutDefinition layoutDefinition = LayoutDefinition.Parse(layout.Value);
          foreach (DeviceDefinition device in layoutDefinition.Devices)
          {
            foreach (RenderingDefinition rendering in device.Renderings)
            {
              Item renderingItem = master.GetItem(new ID(rendering.ItemID));
              string key = renderingItem.Name + " | " + rendering.UniqueId;
              if (!renderingConditions.ContainsKey(key))
              {
                renderingConditions.Add(key, new List<string>());
              }

              List<string> conditions = renderingConditions[key];
              if (rendering.Rules != null)
              {
                XAttribute xAttribute = rendering.Rules.Attribute("pet_active");
                if (xAttribute != null && xAttribute.Value == "1")
                {
                  int ruleIndex = 0;
                  foreach (XNode rule in rendering.Rules.Nodes())
                  {
                    ruleIndex++;
                    var elem = rule as XElement;
                    if (elem != null)
                    {
                      var attribute = elem.Attribute("name");
                      if (attribute != null)
                      {
                        string composedName = ruleIndex.ToString("D2") + "_" + attribute.Value;
                        if (!conditions.Contains(composedName))
                        {
                          conditions.Add(composedName);
                        }
                      }
                    }
                  }
                }
                else
                {
                  renderingsToRemove.Add(renderingItem.ID);
                }
              }
            }
          }
        }
      }

      foreach (DataRow row in gridDataSet.Tables[0].Rows)
      {
        string key = row[0].ToString();

        if (renderingConditions.ContainsKey(key) && renderingConditions[key].Contains(row["Conditions"].ToString()))
        {
          renderingConditions[key].Remove(row["Conditions"].ToString());
        }
      }

      foreach (string renderingName in renderingConditions.Keys)
      {
        foreach (string conditionName in renderingConditions[renderingName])
        {
          gridDataSet.Tables[0].Rows.Add(renderingName, conditionName, 0, 0, 0, 0, 0, 0);
        }
      }

      gridDataSet.Tables[0].DefaultView.Sort = "RenderingName, Conditions";

      if (this.showHistoricalCheckBox.Checked)
      {
        return;
      }

      foreach (ID renderingToRemove in renderingsToRemove)
      {
        for (int rowIndex = gridDataSet.Tables[0].Rows.Count - 1; rowIndex >= 0; rowIndex--)
        {
          DataRow row = gridDataSet.Tables[0].Rows[rowIndex];
          string renderingName = row["RenderingName"].ToString();
          if (renderingName.Equals(renderingToRemove.ToString(), StringComparison.OrdinalIgnoreCase))
          {
            gridDataSet.Tables[0].Rows.RemoveAt(rowIndex);
          }
        }
      }
    }

    protected void RepeaterRowBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var dataItem = (RenderingRecord)e.Item.DataItem;
        var innerRepeater = e.Item.FindControl("innerRepeater") as Repeater;
        if (innerRepeater != null)
        {
          innerRepeater.DataSource = dataItem.Values;
          innerRepeater.DataBind();
        }
      }
    }

    protected void SelectedGoalChanged(object sender, EventArgs args)
    {
      BindData();
    }

    protected void SelectedIndexChanged(object sender, EventArgs args)
    {
      this.goalDropdownPanel.Visible = this.modeRadiogroup.SelectedValue.Equals("Specific goal", StringComparison.OrdinalIgnoreCase);

      updatePanel.Update();

      BindData();
    }

    protected void ShowHistoricalCheckedChanged(object sender, EventArgs args)
    {
      BindData();
    }
  }
}
