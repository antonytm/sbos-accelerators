﻿using Sitecore.Analytics.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.SbosAccelerators.Shared.Analytics.Data;

namespace Sitecore.SbosAccelerators.Shared.Analytics
{
  public static class TrackerHelper
  {
    /// <summary>
    /// This method is an internal access correction for accessing Tracker.FindTrackingField(Item item) method. Code was exported from 6.6 Update-6.
    /// </summary>
    /// <param name="item">The content item to find tracking field of.</param>
    public static TrackingField FindTrackingField(Item item)
    {
      Assert.ArgumentNotNull(item, "item");
      TrackingField trackingField = TrackingFieldHelper.GetTrackingField(item);
      if (trackingField != null)
      {
        return trackingField;
      }

      TemplateItem template = item.Template;
      if (template == null)
      {
        return null;
      }

      return TrackingFieldHelper.GetTrackingField(template.InnerItem);
    }
  }
}
