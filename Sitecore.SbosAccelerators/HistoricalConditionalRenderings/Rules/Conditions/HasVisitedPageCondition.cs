﻿namespace Sitecore.SbosAccelerators.HistoricalConditionalRenderings.Rules.Conditions
{
  # region Usings

  using System;
  using System.Collections.Generic;
  using System.Linq;
  using Sitecore.Analytics.Data.DataAccess.DataSets;
  using Sitecore.Diagnostics;
  using Sitecore.Rules;

  # endregion Usings

  public class HasVisitedPageCondition<T> : HistoricalCondition<T> where T : RuleContext
  {
    # region Fields

    private Guid _pageGuid;

    #endregion Fields

    # region Methods

    protected override bool Execute(T ruleContext)
    {	
      Assert.ArgumentNotNull(ruleContext, "ruleContext");

      try
      {
        this._pageGuid = new Guid(this.PageId);
      }
      catch (Exception)
      {
        Log.Warn(string.Format("Could not convert value to guid: {0}", this.PageId), this.GetType());
        return false;
      }

      IEnumerable<VisitorDataSet.VisitsRow> visits = null;

      switch (this.GetPeriod().ToLower())
      {
        case ("days"):
          visits = this.GetVisitsByDays();
          break;

        case ("visits"):
          visits = this.GetVisits();
          break;
      }

      return visits != null && visits.Any(visit => visit.GetPages().Any(page => page.ItemId == this._pageGuid));
    }

    # endregion Methods

    # region Properties

    public string PageId { get; set; }

    # endregion Properties
  }
}
