﻿namespace Sitecore.SbosAccelerators.MeasurementTypes
{
  using System.Linq;
  using System;
  using Sitecore.Analytics.Data.Items;
  using Sitecore.Analytics.Testing.TestingUtils;
  using Sitecore.Configuration;
  using Sitecore.Data;
  using Sitecore.Data.Fields;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.Events;
  using Sitecore.Globalization;
  using Sitecore.Layouts;
  using Sitecore.SbosAccelerators.MeasurementTypes.Utils;
  using Sitecore.SbosAccelerators.PersonalizationTracker;

  public class ItemEventHandler
  {
    public void OnItemSaved(object sender, EventArgs args)
    {
      try
      {
        if (args != null)
        {
          Item item = Event.ExtractParameter(args, 0) as Item;
          if (item != null)
          {
            if (item.Fields[FieldIDs.LayoutField].HasValue)
            {
              item.Fields.ReadAll();
              string layout = LayoutField.GetFieldValue(item.Fields[FieldIDs.LayoutField]);

              #region MTMVT

              if (layout.Contains("XmvtRenderingID"))
              {
                foreach (DeviceDefinition device in LayoutDefinition.Parse(layout).Devices)
                {
                  foreach (RenderingDefinition rendering in device.Renderings)
                  {
                    if (string.IsNullOrEmpty(rendering.MultiVariateTest))
                    {
                      continue;
                    }

                    // create page events.
                    using (new DatabaseSwitcher(Factory.GetDatabase("master")))
                    {
                      MultivariateTestVariableItem variable = TestingUtil.MultiVariateTesting.GetVariableItem(rendering, Context.ContentDatabase);

                      if (variable == null)
                        continue;

                      Item xmvtCategory = Client.ContentDatabase.GetItem(MtmvtIDs.XmvtCategoryItem, Language.Parse("en"));
                      Item categoryItem;
                      if (xmvtCategory.GetChildren().All(i => !string.Equals(i.Name, item.ID.ToString().Trim(new[] { '{', '}' }), StringComparison.OrdinalIgnoreCase)))
                      {
                        categoryItem = xmvtCategory.Add(item.ID.ToString().Trim(new[]
                        {
                          '{', '}'
                        }), new TemplateID(PageEventCategoryItem.TemplateID));
                        using (new EditContext(categoryItem, true, true))
                        {
                          categoryItem[FieldIDs.DisplayName] = item.Name;
                        }
                      }
                      else
                      {
                        categoryItem = xmvtCategory.Children.FirstOrDefault(i => string.Equals(i.Name, item.ID.ToString().Trim(new[] { '{', '}' }), StringComparison.OrdinalIgnoreCase));
                      }

                      if (categoryItem != null)
                      {
                        PageEventsHelper.CreateMeasurementPageEvents(categoryItem, variable, rendering.UniqueId);
                      }
                    }
                  }
                }
              }

              #endregion

              #region PT

              if (layout.Contains("pet_active"))
              {
                using (new LanguageSwitcher("en"))
                {
                  foreach (DeviceDefinition device in LayoutDefinition.Parse(layout).Devices)
                  {
                    foreach (RenderingDefinition rendering in device.Renderings)
                    {
                      if (rendering.Rules == null)
                      {
                        continue;
                      }

                      Item petCategory = Client.ContentDatabase.GetItem(PetIDs.PetPageEventCategory);

                      Item categoryItem;
                      if (petCategory.GetChildren().All(i => !string.Equals(i.Name, item.ID.ToString().Trim(new[] { '{', '}' }), StringComparison.OrdinalIgnoreCase)))
                      {
                        categoryItem = petCategory.Add(item.ID.ToString().Trim(new[] { '{', '}' }), new TemplateID(PageEventCategoryItem.TemplateID));
                        using (new EditContext(categoryItem, true, true))
                        {
                          categoryItem[FieldIDs.DisplayName] = item.Name;
                        }
                      }
                      else
                      {
                        categoryItem = petCategory.Children.FirstOrDefault(i => string.Equals(i.Name, item.ID.ToString().Trim(new[] { '{', '}' }), StringComparison.OrdinalIgnoreCase));
                      }

                      if (categoryItem != null && !categoryItem.Children.Any(i => i.Name.Contains(rendering.ItemID)))
                      {
                        PageEventsHelper.CreatePersonalizationPageEvents(categoryItem, rendering.UniqueId, rendering.Rules);
                      }
                    }
                  }
                }
              }

              #endregion
            }
          }
        }
      }
      catch (Exception exception)
      {
        Log.Error("[SBOS MTMVT] " + exception.Message, exception, this);
      }
    }
  }
}
