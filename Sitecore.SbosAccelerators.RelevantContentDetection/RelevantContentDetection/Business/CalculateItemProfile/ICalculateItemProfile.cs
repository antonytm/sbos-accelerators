﻿using Sitecore.Analytics.Data.DataAccess.DataSets;
using Sitecore.Analytics.Data.Items;
using Sitecore.SbosAccelerators.RelevantContentDetection.Data;

namespace Sitecore.SbosAccelerators.RelevantContentDetection.Business.CalculateItemProfile
{
    public interface ICalculateItemProfile
    {
        void Calculate(VisitorDataSet.VisitsRow visit, IProfiledItemList list, string profileName, ProfileItem profileItem);
    }
}
