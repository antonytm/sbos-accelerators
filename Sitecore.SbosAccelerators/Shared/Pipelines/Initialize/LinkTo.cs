﻿namespace Sitecore.SbosAccelerators.Shared
{
  using System;
  using System.Web;
  using Sitecore.Collections;
  using Sitecore.Web;
  using Sitecore.Web.UI.HtmlControls;

  public class LinkTo : Page
  {
    protected override void OnLoad(EventArgs e)
    {
      SafeDictionary<string> queryString = WebUtil.ParseQueryString(WebUtil.GetQueryString());

      string redirectTo = queryString["to"];
      string actualRedirectLink = HttpUtility.UrlDecode(redirectTo);

      if (actualRedirectLink != null)
      {
        this.Response.Redirect(actualRedirectLink);
      }
    }
  }
}
