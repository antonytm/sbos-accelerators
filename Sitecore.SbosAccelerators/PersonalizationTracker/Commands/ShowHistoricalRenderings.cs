﻿namespace Sitecore.PET.Commands
{
  using Sitecore.Shell.Framework.Commands;

  public class ShowHistoricalRenderings : Command
  {
    public override void Execute(CommandContext context)
    {
    }

    public override string GetClick(CommandContext context, string click)
    {
      return "ShowHistoricalCheckedChanged";
    }

    //
    // ANP: commented out to compile 
    // todo: find out whether it is required or not
    //
    //public override CommandState QueryState(CommandContext context)
    //{
    //  Sitecore.Web.UI.Sheer.SheerResponse.Alert(PetOptions.ShowHistoricalRenderings.ToString(), true);

    //  if (PetOptions.ShowHistoricalRenderings)
    //  {
    //    return CommandState.Down;
    //  }
    //  return CommandState.Enabled;
    //}
  }
}
