﻿using Sitecore.Analytics.Pipelines.StartTracking;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.IO;
using Sitecore.SecurityModel;

namespace Sitecore.SbosAccelerators.AliasTracker.Analytics
{
  public static class AliasHelper
  {
    /// <summary>
    /// This method is an internal access correction for accessing AliasResolver.[string aliasName] method. Code was exported from 6.6 Update-6.
    /// </summary>
    /// <param name="aliasName">The name of alias to find definition item of.</param>
    public static Item GetAliasItem(string aliasName)
    {
      Assert.ArgumentNotNullOrEmpty(aliasName, "aliasName");

      var database = Sitecore.Context.Database;
      if(database == null)
      {
        Log.Warn("There is no context database in the request of " + Sitecore.Context.Request.FilePath, typeof(AliasHelper));
        return null;
      }  

      var path = FileUtil.MakePath("/sitecore/system/aliases", aliasName, '/');
      return ItemManager.GetItem(path, Language.Invariant, Sitecore.Data.Version.First, database, SecurityCheck.Disable);
    }

    /// <summary>
    /// This method detects if the current content item was requested by an alias and if so then it find the alias definition item.
    /// </summary>
    /// <returns>The alias definition item or null.</returns>
    public static Item GetCurrentAliasItem()
    {
      var aliasName = Sitecore.Context.Request.FilePath;
      return GetAliasItem(aliasName);
    }
  }
}
