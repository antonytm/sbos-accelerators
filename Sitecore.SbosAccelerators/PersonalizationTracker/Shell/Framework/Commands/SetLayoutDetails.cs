namespace Sitecore.SbosAccelerators.PersonalizationTracker.Shell.Framework.Commands
{
  using System;
  using System.Collections.Specialized;
  using Sitecore.Configuration;
  using Sitecore.Data;
  using Sitecore.Data.Fields;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.Globalization;
  using Sitecore.Layouts;
  using Sitecore.Shell.Framework.Commands;
  using Sitecore.Text;
  using Sitecore.Web;
  using Sitecore.Web.UI.Sheer;

  [Serializable]
  public class SetLayoutDetails : Command
  {
    private static void CleanupInheritedItems(Item item)
    {
      Assert.ArgumentNotNull(item, "item");
      Database database = item.Database;
      string query = string.Format("fast:/sitecore/content//*[@@templateid='{0}']", item.TemplateID);
      Item[] itemArray = database.SelectItems(query);
      if (itemArray != null)
      {
        foreach (Item item2 in itemArray)
        {
          Field field = item2.Fields[FieldIDs.LayoutField];
          if (field.HasValue)
          {
            string str2 = CleanupLayoutValue(LayoutField.GetFieldValue(item2.Fields[FieldIDs.LayoutField]));
            item2.Editing.BeginEdit();
            LayoutField.SetFieldValue(item2.Fields[FieldIDs.LayoutField], str2);
            item2.Editing.EndEdit();
          }
        }
      }
    }

    private static string CleanupLayoutValue(string layout)
    {
      Assert.ArgumentNotNull(layout, "layout");
      if (!string.IsNullOrEmpty(layout))
      {
        layout = LayoutDefinition.Parse(layout).ToXml();
      }
      return layout;
    }

    public override void Execute(CommandContext context)
    {
      Assert.ArgumentNotNull(context, "context");
      Error.AssertObject(context, "context");
      if (context.Items.Length == 1)
      {
        Item item = context.Items[0];
        var parameters = new NameValueCollection();
        parameters["id"] = item.ID.ToString();
        parameters["language"] = item.Language.ToString();
        parameters["version"] = item.Version.ToString();
        parameters["database"] = item.Database.Name;
        Context.ClientPage.Start(this, "Run", parameters);
      }
    }

    public override CommandState QueryState(CommandContext context)
    {
      Assert.ArgumentNotNull(context, "context");
      if (context.Items.Length != 1)
      {
        return CommandState.Hidden;
      }
      Item item = context.Items[0];
      if (!this.HasField(item, FieldIDs.LayoutField))
      {
        return CommandState.Hidden;
      }
      if (WebUtil.GetQueryString("mode") == "preview")
      {
        return CommandState.Disabled;
      }
      if (!item.Access.CanWrite())
      {
        return CommandState.Disabled;
      }
      return base.QueryState(context);
    }

    protected void Run(ClientPipelineArgs args)
    {
      Assert.ArgumentNotNull(args, "args");
      if (args.IsPostBack)
      {
        if (args.HasResult)
        {
          Database database = Factory.GetDatabase(args.Parameters["database"]);
          Assert.IsNotNull(database, "Database \"" + args.Parameters["database"] + "\" not found.");
          Item item = database.Items[args.Parameters["id"], Language.Parse(args.Parameters["language"]), Data.Version.Parse(args.Parameters["version"])];
          Assert.IsNotNull(item, "item");
          string result = args.Result;
          item.Editing.BeginEdit();
          if (item.Name != "__Standard Values")
          {
            if (string.Compare(item.Fields[FieldIDs.LayoutField].GetValue(true), result, StringComparison.InvariantCultureIgnoreCase) != 0)
            {
              LayoutDefinition layoutDefinition = LayoutDefinition.Parse(result);
              DeviceDefinition deviceDefinition = layoutDefinition.GetDevice(Context.Device.ID.ToString());

              if (deviceDefinition.Renderings != null)
              {
                foreach (RenderingDefinition renderingDefinition in deviceDefinition.Renderings)
                {
                  if (renderingDefinition.Rules != null)
                  {
                    if (renderingDefinition.Rules.ToString().Contains("pet_active=\"1\""))
                    {
                      if (!renderingDefinition.Parameters.Contains("PetActive"))
                      {
                        renderingDefinition.Parameters += string.IsNullOrEmpty(renderingDefinition.Parameters) ? "PetActive=1" : "&PetActive=1";
                      }
                      else
                      {
                        renderingDefinition.Parameters = renderingDefinition.Parameters.Replace("PetActive=0", "PetActive=1");
                      }
                    }
                    else
                    {
                      if (renderingDefinition.Rules.ToString().Contains("pet_active=\"0\"") && renderingDefinition.Parameters.Contains("PetActive"))
                      {
                        renderingDefinition.Parameters = renderingDefinition.Parameters.Replace("PetActive=1", "PetActive=0");
                      }
                    }
                  }
                }
              }

              LayoutField.SetFieldValue(item.Fields[FieldIDs.LayoutField], layoutDefinition.ToXml());
              string fieldValue = LayoutField.GetFieldValue(item.Fields[FieldIDs.LayoutField]);
              LayoutField.SetFieldValue(item.Fields[FieldIDs.LayoutField], CleanupLayoutValue(fieldValue));
            }
          }
          else
          {
            item[FieldIDs.LayoutField] = CleanupLayoutValue(result);
          }
          item.Editing.EndEdit();
          if (item.Name == "__Standard Values")
          {
            CleanupInheritedItems(item);
          }
          Log.Audit(this, "Set layout details: {0}, layout: {1}", new[] { AuditFormatter.FormatItem(item), result });
        }
      }
      else
      {
        var str4 = new UrlString(UIUtil.GetUri("control:LayoutDetails"));
        str4.Append("id", args.Parameters["id"]);
        str4.Append("la", args.Parameters["language"]);
        str4.Append("vs", args.Parameters["version"]);
        SheerResponse.ShowModalDialog(str4.ToString(), true);
        args.WaitForPostBack();
      }
    }
  }
}
