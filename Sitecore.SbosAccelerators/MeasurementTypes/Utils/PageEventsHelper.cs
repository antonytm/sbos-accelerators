﻿namespace Sitecore.SbosAccelerators.MeasurementTypes.Utils
{
  using System.Linq;
  using System.Xml.Linq;
  using Sitecore.Analytics.Data.Items;
  using Sitecore.Analytics.Testing.TestingUtils;
  using Sitecore.Data;
  using Sitecore.Data.Items;
  using Sitecore.SbosAccelerators.PersonalizationTracker;
  using Sitecore.SecurityModel;

  static class PageEventsHelper
  {
    /// <summary>
    /// Creates page events under the given category for clicks and views.
    /// </summary>
    /// <param name="categoryItem">Category item under /sitecore/System/Settings/Analytics/Page Events node.</param>
    /// <param name="variableItem">MV-testing variable item.</param>
    /// <param name="renderingUniqueId">Rendering unique ID.</param>
    public static void CreateMeasurementPageEvents(Item categoryItem, MultivariateTestVariableItem variableItem, string renderingUniqueId)
    {
      string pattern = renderingUniqueId.Trim(new[] { '{', '}' }) + "_{0}_Click";
      
      foreach (MultivariateTestValueItem child in TestingUtil.MultiVariateTesting.GetVariableValues(variableItem))
      {
        string clickName = string.Format(pattern, child.Name);

        // Click page event creation:
        if (categoryItem == null || categoryItem.Children.Any(item => item.Name == clickName))
        {
          continue;
        }
        
        Item clickItem = categoryItem.Add(clickName, new TemplateID(PageEventItem.TemplateID));

        using (new EditContext(clickItem, true, true))
        {
          clickItem.Fields[FieldIDs.WorkflowState].Value = MtmvtIDs.AnalyticsDeployedWorkflowState.ToString();
          clickItem.Fields[FieldIDs.DisplayName].Value = clickName;
          clickItem.Fields[PageEventItem.FieldIDs.Points].Value = "0";
        }

        PageEventItem clickPageEvent = PageEventItem.Create(clickItem);
        clickPageEvent.BeginEdit();
        clickPageEvent.IsSystem = true;
        clickPageEvent.EndEdit();
      }
    }

    public static void CreatePersonalizationPageEvents(Item categoryItem, string renderingUniqueId, XElement ruleSet)
    {
      string viewItemName  = renderingUniqueId.Trim(new[] { '{', '}' }) + "_{1}_{0}_View";
      string clickItemName = renderingUniqueId.Trim(new[] { '{', '}' }) + "_{1}_{0}_Click";
      
      using (new SecurityDisabler())
      {
        if (ruleSet.HasAttributes)
        {
          var petAttribute = ruleSet.Attribute("pet_active");
          if (petAttribute == null || petAttribute.Value != "1")
          {
            return;
          }
          
          int nodeIndex = 0;
          foreach (XElement node in ruleSet.Nodes())
          {
            nodeIndex++;
            var xAttribute = node.Attribute("uid");
            if (xAttribute == null)
            {
              continue;
            }

            string viewName = string.Format(viewItemName, xAttribute.Value.Trim(new[] { '{', '}' }), nodeIndex.ToString("D2"));
            string clickName = string.Format(clickItemName, xAttribute.Value.Trim(new[] { '{', '}' }), nodeIndex.ToString("D2"));
            var attribute = node.Attribute("name");

            if (categoryItem.Children.All(item => item.Name != viewName))
            {
              Item viewItem = categoryItem.Add(viewName, new TemplateID(PageEventItem.TemplateID));
              PageEventItem viewPageEvent = PageEventItem.Create(viewItem);
              viewPageEvent.BeginEdit();
              viewPageEvent.IsSystem = true;
              viewPageEvent.EndEdit();

              if (attribute != null)
              {
                using (new EditContext(viewItem, true, true))
                {
                  viewItem.Fields[FieldIDs.WorkflowState].Value = PetIDs.AnalyticsDeployedWorkflowState.ToString();
                  viewItem.Fields[FieldIDs.DisplayName].Value = string.Format(viewItemName, attribute.Value.Trim(new[]
                  {
                    '{', '}'
                  }), nodeIndex.ToString("D2"));
                  viewItem.Fields[PageEventItem.FieldIDs.Points].Value = "0";
                  viewItem.Fields[PageEventItem.FieldIDs.Category].Value = "PT";
                }
              }
            }

            if (categoryItem.Children.All(item => item.Name != clickName))
            {
              Item clickItem = categoryItem.Add(clickName, new TemplateID(PageEventItem.TemplateID));
              PageEventItem clickPageEvent = PageEventItem.Create(clickItem);
              clickPageEvent.BeginEdit();
              clickPageEvent.IsSystem = true;
              clickPageEvent.EndEdit();
              
              if (attribute != null)
              {
                using (new EditContext(clickItem, true, true))
                {
                  clickItem.Fields[FieldIDs.WorkflowState].Value = PetIDs.AnalyticsDeployedWorkflowState.ToString();
                  clickItem.Fields[FieldIDs.DisplayName].Value = string.Format(clickItemName, attribute.Value.Trim(new[] { '{', '}' }), nodeIndex.ToString("D2"));
                  clickItem.Fields[PageEventItem.FieldIDs.Points].Value = "0";
                  clickItem.Fields[PageEventItem.FieldIDs.Category].Value = "PT";
                }
              }
            }
          }
        }
      }
    }
  }
}
