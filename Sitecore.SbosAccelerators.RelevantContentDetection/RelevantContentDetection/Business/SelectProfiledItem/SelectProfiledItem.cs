﻿using Sitecore.SbosAccelerators.RelevantContentDetection.Business.CalculateItemProfile;
using Sitecore.SbosAccelerators.RelevantContentDetection.Data;

namespace Sitecore.SbosAccelerators.RelevantContentDetection.Business.SelectProfiledItem
{
    public abstract class SelectProfiledItem : ISelectProfiledItem
    {
        public SelectProfiledItem(ICalculateItemProfile calc)
        {
            CalculateItemProfile = calc;
        }

        public abstract IProfiledItemResult GetProfiledItem(IProfiledItemList list);

        public abstract string StringKey { get; }

        public ICalculateItemProfile CalculateItemProfile { get; private set; }

    }

    
}
