namespace Sitecore.SbosAccelerators.Shared.Presentation
{
  using System;
  using System.Collections.Generic;
  using Sitecore.Presentation;

  public class ControlProvider : Sitecore.Presentation.ControlProvider
  {
    protected override ControlUriParser GetControlUriParser(string controlUri, Dictionary<string, string> parameters)
    {
      if (controlUri.StartsWith("xsl://", StringComparison.InvariantCulture))
      {
        return new XslControlUriParser(controlUri, parameters);
      }
      return null;
    }
  }
}
