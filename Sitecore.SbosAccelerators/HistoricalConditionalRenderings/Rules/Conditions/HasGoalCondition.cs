﻿namespace Sitecore.SbosAccelerators.HistoricalConditionalRenderings.Rules.Conditions
{
  # region Usings

  using System;
  using System.Collections.Generic;
  using System.Linq;
  using Sitecore.Analytics;
  using Sitecore.Analytics.Data.DataAccess;
  using Sitecore.Analytics.Data.DataAccess.DataSets;
  using Sitecore.Diagnostics;
  using Sitecore.Rules;

  # endregion Usings

  class HasGoalCondition<T> : HistoricalCondition<T> where T : RuleContext
  {
    # region Fields

    private Guid _goalGuid;

    #endregion Fields

    # region Methods

    protected override bool Execute(T ruleContext)
    {
      Assert.ArgumentNotNull(ruleContext, "ruleContext");

      try
      {
        this._goalGuid = new Guid(this.GoalId);
      }
      catch (Exception)
      {
        Log.Warn(string.Format("Could not convert value to guid: {0}", this.GoalId), this.GetType());
        return false;
      }

      IEnumerable<VisitorDataSet.VisitsRow> visits = null;
      
      switch (this.GetPeriod().ToLower())
      {
        case ("days"):
          visits = this.GetVisitsByDays();
          break;

        case ("visits"):
          visits = this.GetVisits();
          break;
      }

      if (visits == null) return false;

      var events = GetEvents(visits.Last().StartDateTime);

      return visits.Any(visit => events.Where(@event => @event.VisitId == visit.VisitId).Any(@event => @event.PageEventDefinitionId == this._goalGuid));
    }

    private static IEnumerable<VisitorDataSet.PageEventsRow> GetEvents(DateTime olderDate)
    {
      var visitor = Tracker.Visitor;
      
      var visitorLoadOptions = new VisitorLoadOptions
      {
        Count = visitor.VisitCount,
        VisitLoadOptions = VisitLoadOptions.PageEvents,
        NotOlderThan = olderDate,
        Start = 1
      };

      visitor.Load(visitorLoadOptions);
      var events = visitor.DataSet.PageEvents;

      return events.Where(@event => @event.DateTime >= olderDate).OrderByDescending(@event => @event.DateTime);
    }

    # endregion Methods

    # region Properties

    public string GoalId { get; set; }

    # endregion Properties
  }
}
