﻿namespace Sitecore.SbosAccelerators.PersonalizationTracker.Caching
{
  using System;
  using System.Collections.Generic;
  using System.Data;

  public static class DataSetCache
  {
    public struct CachedDataSet
    {
      public DateTime CreatedAt;
      public DataSet DataSet;

      public CachedDataSet(DateTime dateTime, DataSet dataSet)
      {
        this.CreatedAt = dateTime;
        this.DataSet = dataSet;
      }
    }
    
    public static volatile Dictionary<string, Dictionary<string, CachedDataSet>> PerItemCache = new Dictionary<string, Dictionary<string, CachedDataSet>>();
  }
}
