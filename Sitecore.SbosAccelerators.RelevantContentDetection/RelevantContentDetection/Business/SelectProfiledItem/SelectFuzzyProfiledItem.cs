﻿using Sitecore.SbosAccelerators.RelevantContentDetection.Business.CalculateItemProfile;
using Sitecore.SbosAccelerators.RelevantContentDetection.Data;

namespace Sitecore.SbosAccelerators.RelevantContentDetection.Business.SelectProfiledItem
{
    

    public abstract class SelectFuzzyProfiledItem: SelectProfiledItem, ISelectFuzzyProfiledItem
    {
        public SelectFuzzyProfiledItem(ICalculateItemProfile calc) : base(calc)
        {
        }

        public abstract IProfiledItemResult GetFuzzyProfiledItem(IProfiledItemList list);
    }
}
