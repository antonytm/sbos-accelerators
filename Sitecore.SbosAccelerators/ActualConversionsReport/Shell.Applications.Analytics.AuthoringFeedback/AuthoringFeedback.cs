﻿using System.Linq;
using Sitecore.Analytics.Data.Items;
using Sitecore.Data.Items;
using Sitecore.StringExtensions;
using Sitecore.Text;
using Sitecore.Web;
using Sitecore.Web.UI.Sheer;
using System;
using Sitecore.Data;
using Sitecore.SbosAccelerators.MeasurementTypes.Enums;
using Sitecore.SbosAccelerators.MeasurementTypes.Utils;

namespace Sitecore.SbosAccelerators.ActualConversionsReport.Shell.Applications.Analytics.AuthoringFeedback
{
  public class AuthoringFeedback : Sitecore.Shell.Applications.Analytics.AuthoringFeedback.AuthoringFeedback
  {
    public override void Execute(Sitecore.Shell.Framework.Commands.CommandContext context)
    {
      var item = context.Items.First();
      var id = item.ID;
      if (id == ItemIDs.Analytics.MarketingCenter.TestLaboratory)
      {
        // display general Latest Tests report
        this.DisplayLatestTests();
      }
      else
      {
        var templateId = item.TemplateID;
        if (templateId == MultivariateTestDefinitionItem.TemplateID)
        {
          // display test details report
          this.DisplayTestDetails(item);
        }
        else if (templateId == MultivariateTestVariableItem.TemplateID)
        {
          // display test variation details report
          this.DisplayTestDetails(item.Parent);
        }
        else if (templateId == MultivariateTestValueItem.TemplateID)
        {
          // display test variation details report
          this.DisplayTestVariationDetails(item);
        }
        else
        {
          // use base behavior 
          base.Execute(context);
        }
      }
    }

    protected virtual void DisplayLatestTests()
    {
      DisplayReport(MtmvtIDs.ReportIDs.LatestTests.ToString(), "l=1");
    }

    protected virtual void DisplayTestDetails(Item item)
    {
      string measurementTypeId = item[MtmvtIDs.MeasurementTypeField];

      if (string.IsNullOrEmpty(measurementTypeId))
      {
        DisplayReport(MtmvtIDs.ReportIDs.ByComponentEngagement.ToString(), "testid=" + item.ID);
        return;
      }

      MeasurementType measurement = (MeasurementType)Enum.Parse(typeof(MeasurementType), Context.ContentDatabase.GetItem(ID.Parse(measurementTypeId)).Name);

      switch (measurement)
      {
        default:
          DisplayReport(MtmvtIDs.ReportIDs.ByComponentEngagement.ToString(), "testid=" + item.ID);
          break;
        case MeasurementType.SpecificGoal:
          string goalId = item[MtmvtIDs.SelectedGoalField];
          DisplayReport(MtmvtIDs.ReportIDs.ByComponentConversions.ToString(), "testid=" + item.ID + "&goalId=" + goalId);
          break;
        case MeasurementType.ClickThroughRate:
          DisplayReport(MtmvtIDs.ReportIDs.ByComponentCtr.ToString(), "testid=" + item.ID);
          break;
      }
    }

    protected virtual void DisplayTestVariationDetails(Item item)
    {
      var variantIndex = GetIndex(item);
      var componentIndex = GetIndex(item.Parent);

      DisplayReport(MtmvtIDs.ReportIDs.VariantDetail.ToString(), "p=1&testid=" + item.Parent.ParentID + "&componentIndex=" + componentIndex + "&variantIndex=" + variantIndex);
    }

    private static int GetIndex(Item item)
    {
      int variantIndex = -1;
      foreach (Item child in item.Parent.Children)
      {
        variantIndex++;
        if (child.ID == item.ID)
        {
          break;
        }
      }
      return variantIndex;
    }

    protected virtual void DisplayReport(string reportId, object extraParameters)
    {
      if (WebUtil.GetFormValue("scEditorTabs").Contains("analytics:authoringfeedback"))
      {
        SheerResponse.Eval("scContent.onEditorTabClick(null, null, 'OpenAuthoringFeedback')");
      }
      else
      {
        Item reportsItem = Client.ContentDatabase.GetItem(ItemIDs.Analytics.Reports.ItemReports);
        if (reportsItem == null)
        {
          SheerResponse.Alert("You do not have permission to see any item reports.", new string[0]);
        }
        else
        {
          var urlString = new UrlString("/sitecore/shell/applications/analytics/default.aspx?fo={0}&ro={0}{1}".FormatWith(reportId, extraParameters != null ? "&" + extraParameters : string.Empty));
          UIUtil.AddContentDatabaseParameter(urlString);
          const string jsCode = "window.open('{0}','_blank')";
          SheerResponse.Eval(jsCode.FormatWith(urlString));
        }
      }
    }
  }
}