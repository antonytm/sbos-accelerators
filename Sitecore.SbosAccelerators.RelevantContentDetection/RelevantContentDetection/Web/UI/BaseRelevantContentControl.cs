﻿using Sitecore.Analytics;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;

namespace Sitecore.SbosAccelerators.RelevantContentDetection.Web.UI
{
  using System;
  using System.Collections.Generic;
  using System.Collections.Specialized;
  using System.Linq;
  using System.Web.UI;
  using Sitecore.SbosAccelerators.RelevantContentDetection.Business.SelectProfiledItem;
  using Sitecore.SbosAccelerators.RelevantContentDetection.Data;

  public class BaseRelevantContentControl : UserControl
  {
    public const string DefaultDetectionAlgorithm = SelectClosestItem.stringKey;

    Dictionary<string, ISelectFuzzyProfiledItem> SelectProfiledItemMap = new Dictionary<string, ISelectFuzzyProfiledItem>();

    public BaseRelevantContentControl()
    {
      Init += RelevantContentControl_Init;
      Load += RelevantContentControl_Load;

      CreateDetectionAlgorithmMap();

    }

    private void CreateDetectionAlgorithmMap()
    {
      SelectProfiledItemMap.Add(SelectClosestItem.stringKey.ToLowerInvariant(), new SelectClosestItem());
      SelectProfiledItemMap.Add(SelectFuzzyWeightedItem.stringKey.ToLowerInvariant(), new SelectFuzzyWeightedItem());
      SelectProfiledItemMap.Add(SelectFuzzyAssociationItem.stringKey.ToLowerInvariant(), new SelectFuzzyAssociationItem());
    }


    #region Lifecycle Events

    void RelevantContentControl_Init(object sender, EventArgs e)
    {
      try
      {
        Initialize();
      }
      catch (Exception ex)
      {
        RelevantContentMessages.Add(ex.Message);
        Log.Warn("BaseRelevantContentControl threw an exception", ex, this);
      }
    }

    private void Initialize()
    {
      Assert.IsNotNullOrEmpty(DataSource, "DataSource Rendering Parameter Cannot Be Null or Empty");
      // Assert.IsNotNullOrEmpty(DefaultContentDataSource, string.Format("DefaultContentDataSource Rendering Parameter cannot be null or empty"));
      Assert.IsNotNullOrEmpty(ProfileId, string.Format("ProfileId Rendering Parameter cannot be null or empty"));
      Assert.IsNotNullOrEmpty(DetectionAlgorithm, string.Format("DetectionAlgorithm Rendering Parameter cannot be null or empty"));

      var db = Sitecore.Context.Database;

      // get datasource item
      var contentFolderDataSourceItem = db.GetItem(DataSource);
      Assert.IsNotNull(contentFolderDataSourceItem, string.Format("DataSource item [{0}] not found", DataSource));

      // get datasource item children
      var contentFolderItemList = contentFolderDataSourceItem.Children.ToList();
      Assert.IsTrue((contentFolderItemList.Count > 0), string.Format("DataSource [{0}] has no children items", DataSource));

      // get default data source item
      //var defaultContentDataSourceId = new Sitecore.Data.ID(DefaultContentDataSource);
      //var defaultContentDataSourceItem = db.GetItem(defaultContentDataSourceId);
      // Assert.IsNotNull(defaultContentDataSourceItem, string.Format("DefaultContentDataSource item [{0}] not found", DefaultContentDataSource));

      // get relevant profile item
      var relevantProfileItem = db.GetItem(ProfileId);
      Assert.IsNotNull(relevantProfileItem, string.Format("Profile item [{0}] not found", ProfileId));
      var relevantProfileName = relevantProfileItem.Name;
      Assert.IsNotNullOrEmpty(relevantProfileName, string.Format("Profile item [{0}] name is empty", ProfileId));

      // Get the profile definition
      var profileItem = Tracker.DefinitionItems.Profiles[relevantProfileName];
      Assert.IsNotNull(profileItem, string.Format("Profile [{0}] is not defined.", relevantProfileName));

      // create profiled item list
      ProfiledItemlist = new ProfiledItemList(contentFolderItemList);
      Assert.IsNotNull(ProfiledItemlist, string.Format("Failed to Create Profiled Item List"));

      // get profile detection algorithm
      var selectProfiledItemAlgorithm = SelectProfiledItemMap[DetectionAlgorithm.ToLowerInvariant()];
      Assert.IsNotNull(selectProfiledItemAlgorithm, string.Format("Select Profiled Item Algorithm [{0}] not found", DetectionAlgorithm));

      // calculate profiled item profile
      selectProfiledItemAlgorithm.CalculateItemProfile.Calculate(Tracker.CurrentVisit, ProfiledItemlist, relevantProfileName, profileItem);

      // add message
      var profiledItemCount = ProfiledItemlist.Count(item => (item.ContentProfile != null));
      var profileMessage = string.Format("Found {0} items for profile {1}", profiledItemCount, relevantProfileName);
      RelevantContentMessages.Add(profileMessage);

      // select relevant profiled item 
      IProfiledItemResult relevantProfiledResult;

      if (PageEditEnabled)
      {
        relevantProfiledResult = selectProfiledItemAlgorithm.GetProfiledItem(ProfiledItemlist);
      }
      else
      {
        relevantProfiledResult = selectProfiledItemAlgorithm.GetFuzzyProfiledItem(ProfiledItemlist);
      }

      var relevantProfiledItem = relevantProfiledResult.ProfiledItem;
      var relevantProfiledMessage = relevantProfiledResult.Message;
      RelevantContentMessages.Add(relevantProfiledMessage);

      if (relevantProfiledItem as ProfiledItem == null && DefaultContentDataSourceItem != null)
      {
        var getmessage = string.Format("Displaying default data source");
        RelevantContentMessages.Add(getmessage);
        RelevantContentItem = new ProfiledItem(DefaultContentDataSourceItem);
      }
      else if (relevantProfiledItem as ProfiledItem == null && ProfiledItemlist.Count > 0)
      {
        var getmessage = string.Format("No Default Content Data Source set displaying first datasource child item");
        RelevantContentMessages.Add(getmessage);
        RelevantContentItem = ProfiledItemlist.FirstOrDefault() as ProfiledItem;
      }
      else if (relevantProfiledItem as ProfiledItem == null)
      {
        throw new Exception("Invalid DataSource and Default Content Data Source rendering parameters");
      }
      else
      {
        RelevantContentItem = relevantProfiledItem as ProfiledItem;
      }
    }


    void RelevantContentControl_Load(object sender, EventArgs e)
    {

    }

    #endregion

    #region Basic Properties

    public Item Item
    {
      get { return (RelevantContentItem != null ? RelevantContentItem.Item : null) ?? DefaultContentDataSourceItem; }
    }

    public IProfiledItemList ProfiledItemlist
    {
      get
      {
        return _profiledItemList;
      }
      protected set
      {
        _profiledItemList = value;
      }
    }
    private IProfiledItemList _profiledItemList = null;


    public ProfiledItem RelevantContentItem
    {
      get
      {
        return _relevantContentItem;
      }
      protected set
      {
        _relevantContentItem = value;
      }
    }
    private ProfiledItem _relevantContentItem = null;

    public List<string> RelevantContentMessages
    {
      get { return _relevantContentMessages; }
    }
    private List<string> _relevantContentMessages = new List<string>();

    public new Sitecore.Web.UI.WebControl Parent
    {
      get
      {
        return (Sitecore.Web.UI.WebControl)base.Parent;
      }
    }

    public string DataSource
    {
      get
      {
        return Parent.DataSource;
      }
    }

    #endregion

    #region Extended Properties

    public NameValueCollection Parameters
    {
      get
      {
        if (_parameters == null)
        {
          _parameters = Sitecore.Web.WebUtil.ParseUrlParameters(Parent.Parameters);
        }
        return _parameters;
      }
    }
    private NameValueCollection _parameters = null;

    public String ProfileId
    {
      get
      {
        if (_profileId == null)
        {
          _profileId = Parameters["ProfileId"] ?? string.Empty;
        }
        return _profileId;
      }
      set
      {
        _profileId = value ?? string.Empty;
      }
    }
    private String _profileId = null;

    public String DefaultContentDataSource
    {
      get
      {
        if (_defaultContentDataSource == null)
        {
          _defaultContentDataSource = Parameters["DefaultContentDataSource"] ?? string.Empty;
        }
        return _defaultContentDataSource;
      }
      set
      {
        _defaultContentDataSource = value ?? string.Empty;
        _defaultContentDataSourceItem = null;
      }
    }
    private String _defaultContentDataSource = null;

    public Item DefaultContentDataSourceItem
    {
      get
      {
        if (_defaultContentDataSourceItem == null && !string.IsNullOrEmpty(DefaultContentDataSource))
        {
          var defaultContentDataSourceId = new Sitecore.Data.ID(DefaultContentDataSource);
          _defaultContentDataSourceItem = Sitecore.Context.Database.GetItem(defaultContentDataSourceId);
        }
        return _defaultContentDataSourceItem;
      }
    }
    private Item _defaultContentDataSourceItem = null;

    public String DetectionAlgorithm
    {
      get
      {
        if (_detectionAlgorithm == null)
        {
          _detectionAlgorithm = Parameters["DetectionAlgorithm"] ?? DefaultDetectionAlgorithm;
        }
        return _detectionAlgorithm;
      }
      set
      {
        _detectionAlgorithm = value ?? DefaultDetectionAlgorithm;
      }
    }
    private String _detectionAlgorithm = null;
    // DetectionAlgorithm: weight || distance

    public Boolean ShowDebug
    {
      get
      {
        if (_showDebug == null)
        {
          _showDebug = (Parameters["ShowDebug"] == "1");
        }
        return _showDebug.GetValueOrDefault(false);
      }
      set
      {
        _showDebug = value;
      }
    }
    private Nullable<Boolean> _showDebug = null;
    // DetectionAlgorithm: weight || distance



    public bool PageEditEnabled
    {
      get
      {
        return (Sitecore.Context.Site.DisplayMode == Sitecore.Sites.DisplayMode.Edit);
      }
    }

    #endregion
  }
}
