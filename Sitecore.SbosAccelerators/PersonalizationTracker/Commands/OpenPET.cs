namespace Sitecore.SbosAccelerators.PersonalizationTracker.Commands
{
  using System.Collections;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.Layouts;
  using Sitecore.Shell.Applications.WebEdit.Commands;
  using Sitecore.Shell.Framework.Commands;
  using Sitecore.Text;
  using Sitecore.Web.UI.Sheer;

  public class OpenPET : WebEditCommand
  {
    public override void Execute(CommandContext context)
    {
      Assert.ArgumentNotNull(context, "context");
      if (context.Items.Length == 1)
      {
        Item item = context.Items[0];
        var str = new UrlString("/sitecore/shell/default.aspx?xmlcontrol=PETForm");
        str["fo"] = item.ID.ToString();
        str["la"] = item.Language.ToString();
        str["vs"] = item.Version.ToString();
        SheerResponse.Eval(string.Concat(new object[] { "window.open('", str, "', 'SitecoreWebEditEditor', 'location=0,menubar=0,status=0,toolbar=0,resizable=1')" }));
      }
    }
  }
}