// -----------------------------------------------------------------------
// <copyright file="Testing.cs" company="Sitecore A/S">
// Copyright � 2013 by Sitecore A/S
// </copyright>
// -----------------------------------------------------------------------

namespace Sitecore.SbosAccelerators.MeasurementTypes.Analytics.Pipelines.InsertRenderings
{
  using System;
  using System.Collections.Generic;
  using System.Reflection;
  using System.Text.RegularExpressions;
  using Sitecore.Analytics;
  using Sitecore.Analytics.Data.Items;
  using Sitecore.Analytics.Testing;
  using Sitecore.Data;
  using Sitecore.Data.Items;
  using Sitecore.Diagnostics;
  using Sitecore.Layouts;
  using Sitecore.Pipelines.InsertRenderings;
  using Sitecore.SecurityModel;
  using Sitecore.StringExtensions;

  /// <summary>
  /// Replaces the default Testing processor is several measurement types are supported.
  /// </summary>
  public class Testing : Sitecore.Analytics.Pipelines.InsertRenderings.Testing
  {
    /// <summary>
    /// Performs main processing.
    /// </summary>
    /// <param name="args">Insert rendering pipeline arguments.</param>
    public override void Process(InsertRenderingsArgs args)
    {
      Assert.ArgumentNotNull(args, "args");
      if ((((Tracker.IsActive && args.EvaluateConditions) && args.HasRenderings) && !args.IsDesigning) && !Context.PageMode.IsPageEditor)
      {
        Item contextItem = args.ContextItem;
        if (contextItem != null)
        {
          this.Evaluate(args, contextItem);
        }
      }
    }

    /// <summary>
    /// Evaluates testing rules for the given item.
    /// </summary>
    /// <param name="args">Insert renderings pipeline arguments.</param>
    /// <param name="contextItem">Context item to evaluate.</param>
    protected override void Evaluate(InsertRenderingsArgs args, Item contextItem)
    {
      using (new SecurityDisabler())
      {
        List<RenderingReference> renderings = new List<RenderingReference>(args.Renderings);
        for (int i = renderings.Count - 1; i >= 0; i--)
        {
          RenderingReference rendering = renderings[i];
          string multiVariateTest = rendering.Settings.MultiVariateTest;
          if (!string.IsNullOrEmpty(multiVariateTest))
          {
            Item variableItem = contextItem.Database.GetItem(multiVariateTest);
            if ((variableItem != null) && this.Test(renderings, rendering, contextItem, variableItem))
            {
              var testingRenderingUniqueIds = args.GetType().InvokeMember("TestingRenderingUniqueIds", BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic, Type.DefaultBinder, args, null) as List<string>;
              Assert.IsNotNull(testingRenderingUniqueIds, "[XMVT] args.TestingRenderingUniqueIds is null");
              testingRenderingUniqueIds.Add(rendering.UniqueId);
            }
          }
        }

        args.Renderings.Clear();
        args.Renderings.AddRange(renderings);
      }
    }

    /// <summary>
    /// Gets test combination for the given variable item.
    /// </summary>
    /// <param name="variableItem">Item to get test combination for.</param>
    /// <returns>A test combination for the given item.</returns>
    private TestCombination GetTestCombination(Item variableItem)
    {
      Item parent = variableItem.Parent;
      TestDefinitionItem testDefinitionItem = null;
      if (!TestDefinitionItem.TryParse(parent, ref testDefinitionItem))
      {
        return null;
      }

      if (!testDefinitionItem.IsRunning)
      {
        return null;
      }

      if (!Tracker.CurrentPage.IsTestSetIdNull() && (Tracker.CurrentPage.TestSetId == testDefinitionItem.ID.Guid))
      {
        try
        {
          return Tracker.CurrentPage.GetTestCombination();
        }
        catch
        {
          TestSet testSet = TestManager.GetTestSet(testDefinitionItem);
          TestCombination testCombination = TestManager.GetTestStrategy(testDefinitionItem).GetTestCombination(testSet);
          if (Tracker.CurrentPage.IsTestSetIdNull())
          {
            testCombination.SaveTo(Tracker.CurrentPage);
          }

          return testCombination;
        }
      }

      TestSet testSetOuter = TestManager.GetTestSet(testDefinitionItem);
      TestCombination testCombinationOUter = TestManager.GetTestStrategy(testDefinitionItem).GetTestCombination(testSetOuter);
      if (Tracker.CurrentPage.IsTestSetIdNull())
      {
        testCombinationOUter.SaveTo(Tracker.CurrentPage);
      }

      return testCombinationOUter;
    }

    /// <summary>
    /// Determines whether MV testing on the given components succeeds.
    /// </summary>
    /// <param name="renderings">A list of supplementary renderings to test.</param>
    /// <param name="rendering">A main rendering to test.</param>
    /// <param name="contextItem">The context item.</param>
    /// <param name="variableItem">An item of test variable.</param>
    /// <returns>True if test passes, and false otherwise.</returns>
    private bool Test(List<RenderingReference> renderings, RenderingReference rendering, Item contextItem, Item variableItem)
    {
      TestCombination testCombination = this.GetTestCombination(variableItem);
      if (testCombination == null)
      {
        return false;
      }

      Guid guid = testCombination[variableItem.ID.ToGuid()];
      MultivariateTestValueItem variation = (MultivariateTestValueItem)variableItem.Children[guid.ToString()];
      if (variation == null)
      {
        return false;
      }

      ComponentTestRunner runner = new ComponentTestRunner();
      ComponentTestContext context = new ComponentTestContext(variation, rendering, renderings);
      try
      {
        runner.Run(context);

        Context.ClientData.SetValue(rendering.UniqueId, variation.Name);

        if (Tracker.IsActive)
        {
          Context.ClientData.SetValue(rendering.UniqueId, "{0}_{1}".FormatWith(variation.Name, Tracker.CurrentVisit.CurrentPage.PageId.ToString()));
        }
      }
      catch (Exception exception)
      {
        ID id = rendering.RenderingID ?? ID.Null;
        Log.Warn("Failed to execute MV testing on component with id \"{0}\". Item ID:\"{1}\"".FormatWith(new object[] { id, contextItem.ID }), exception, this);
      }

      return true;
    }
  }
}
