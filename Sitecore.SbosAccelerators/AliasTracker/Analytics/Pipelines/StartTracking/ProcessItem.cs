﻿using Sitecore.Analytics.Data;
using Sitecore.Analytics.Pipelines.StartTracking;
using Sitecore.Data.Items;

namespace Sitecore.SbosAccelerators.AliasTracker.Analytics.Pipelines.StartTracking
{
  public class ProcessItem : Sitecore.Analytics.Pipelines.StartTracking.ProcessItem
  {
    public override void Process(StartTrackingArgs args)
    {
      // to trigger analytics data assigned to the alias item only
      this.ProcessAlias(args);

      // to trigger analytics data assigned to the real content item
      base.Process(args);
    }

    protected virtual void ProcessAlias(StartTrackingArgs args)
    {
      var aliasItem = AliasHelper.GetCurrentAliasItem();
      if (aliasItem != null)
      {
        Process(aliasItem);
      }
    }

    /// <summary>
    /// This method was exporeted from base.Process(StartTrackingArgs args) method. Code was exported from 6.6 Update-6.
    /// </summary>
    /// <param name="aliasItem">The alias definition item.</param>
    protected virtual void Process(Item aliasItem)
    {
      new TrackingFieldProcessor().Process(aliasItem);
    }
  }
}
