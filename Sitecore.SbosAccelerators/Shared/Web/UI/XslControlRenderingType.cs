namespace Sitecore.SbosAccelerators.Shared.Web.UI
{
  using System.Collections.Specialized;
  using System.Web.UI;
  using Sitecore.SbosAccelerators.Shared.Web.UI.WebControls;
  using Sitecore.Reflection;

  public class XslControlRenderingType : Sitecore.Web.UI.XslControlRenderingType
  {
    public override Control GetControl(NameValueCollection parameters, bool assert)
    {
      var control = new XslFile();
      foreach (string str in parameters.Keys)
      {
        string str2 = parameters[str];
        ReflectionUtil.SetProperty(control, str, str2);
      }

      return control;
    }
  }
}
