﻿namespace Sitecore.SbosAccelerators.MeasurementTypes.Enums
{
  public enum MeasurementType
  {
    EngagementValue,
    SpecificGoal,
    ClickThroughRate
  }
}
